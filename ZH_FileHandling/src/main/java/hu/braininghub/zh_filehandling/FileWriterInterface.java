package hu.braininghub.zh_filehandling;

import java.util.List;

/**
 *
 * @author Zsolt
 */
public interface FileWriterInterface {

    public void writeToFileEvrySecondLines(String fileName, List<String> lines);
}
