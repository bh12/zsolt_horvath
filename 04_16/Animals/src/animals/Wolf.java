package animals;

public class Wolf extends Animal implements AttackInterface{

    public Wolf(int age) {
        super("wolf", true, true, false, true);
        setAge(age);
        setLive(true);
        setLiveScore(400);
    }

    @Override
    public void sayHello() {
        System.out.println("VAUVAU");
    }

    @Override
    public String getDeathScream() {
        return "VAUVAUVAU \n";
    }

    @Override
    public int getAttackScore() {
        return 20;
    }

    @Override
    public void attack() {
        System.out.println("\t\t\t Wolf's bite !  +  Wolf's livescore: " + getLiveScore());
    }
}
