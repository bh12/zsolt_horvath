package zh2.swing.vehicle.controller;
//@author Zsolt Horváth

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import static zh2.swing.vehicle.Main.option;
import static zh2.swing.vehicle.Main.sc;

import zh2.swing.vehicle.model.Model;

import zh2.swing.vehicle.view.View;

public class Controller {

    private LocalDateTime localDateTime;
    private View view;
    private Model model;

    public Controller() {
        this.view = new View(this);
        this.model = new Model();
        view.init();
    }

    public void handleCreatBmwButton() {
        model.creatBMW();
    }

    public void handleCreatAirplaneButton() {
        model.creatAirplane();
        logToAreaAirplane();
    }

    public void logToAreaBmw() {
        this.localDateTime = LocalDateTime.now();
        view.getText().append(model.getVehicle().size() + ". " + localDateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")) + " Bmw created with " + model.getBmw().makeASerialNumber() + "#ID" + "\n");

    }

    public void logToAreaAirplane() {
        this.localDateTime = LocalDateTime.now();
        view.getText().append(model.getVehicle().size() + ". " + localDateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")) + " Airplane created with " + model.getAirplane().makeASerialNumber() + "#ID" + "\n");

    }
    
    public void logToConsole(){
        this.localDateTime = LocalDateTime.now();
        System.out.println(model.getVehicle().size() + ". " + localDateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")) + " Airplane created with " + model.getAirplane().makeASerialNumber() + "#ID" + "\n");
    }

    public void startApp() {
        do {
            System.out.println("Kérem válasszon az alábbi lehetőségek közül:");
            System.out.println("1: Új BMW létrehozása");
            System.out.println("2: Új repülő léterhozása");
            System.out.println("3: Kilépés.");

            option = sc.nextInt();

            switch (option) {
                case 1:
                    handleCreatBmwButton();
                    logToConsole();
                    logToAreaBmw();

                    break;
                case 2:
                    handleCreatAirplaneButton();

                    break;
                case 3:
                    break;

            }
        } while (option != 3);
    }

}
