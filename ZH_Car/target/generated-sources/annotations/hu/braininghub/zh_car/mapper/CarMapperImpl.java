package hu.braininghub.zh_car.mapper;

import hu.braininghub.zh_car.dto.CarDTO;
import hu.braininghub.zh_car.entity.CarEntity;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-09-20T16:33:36+0200",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 1.8.0_241 (Oracle Corporation)"
)
public class CarMapperImpl implements CarMapper {

    @Override
    public CarDTO toDTO(CarEntity userEntity) {
        if ( userEntity == null ) {
            return null;
        }

        CarDTO carDTO = new CarDTO();

        carDTO.setPlateNumber( userEntity.getPlateNumber() );
        carDTO.setBrand( userEntity.getBrand() );
        carDTO.setMileage( userEntity.getMileage() );

        return carDTO;
    }

    @Override
    public CarEntity toEntity(CarDTO carDTO) {
        if ( carDTO == null ) {
            return null;
        }

        CarEntity carEntity = new CarEntity();

        carEntity.setBrand( carDTO.getBrand() );
        carEntity.setMileage( carDTO.getMileage() );
        carEntity.setPlateNumber( carDTO.getPlateNumber() );

        return carEntity;
    }

    @Override
    public List<CarDTO> toDTOList(List<CarEntity> carEntities) {
        if ( carEntities == null ) {
            return null;
        }

        List<CarDTO> list = new ArrayList<CarDTO>( carEntities.size() );
        for ( CarEntity carEntity : carEntities ) {
            list.add( toDTO( carEntity ) );
        }

        return list;
    }
}
