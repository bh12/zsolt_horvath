package zh_ikeam;
//@author Zsolt Horváth

public class Fun extends Product implements CanTurnOnOff {

    int turnCounter = 0;

    @Override
    public void turnOn(boolean turn) {

        if (turn) {
            turnCounter++;
        }
    }

    @Override
    public void turnControll() {

        if (turnCounter == 5) {
            System.out.println("A termék tönkrement");
        }
    }

    @Override
    public int getTurnCount() {
        return turnCounter;
    }

}
