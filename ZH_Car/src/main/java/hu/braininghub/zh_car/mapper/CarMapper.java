package hu.braininghub.zh_car.mapper;

import hu.braininghub.zh_car.dto.CarDTO;
import hu.braininghub.zh_car.entity.CarEntity;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 *
 * @author Zsolt
 */
@Mapper
public interface CarMapper {

    CarMapper INSTANCE = Mappers.getMapper(CarMapper.class);

    CarDTO toDTO(CarEntity userEntity);

    CarEntity toEntity(CarDTO carDTO);

    List<CarDTO> toDTOList(List<CarEntity> carEntities);
}
