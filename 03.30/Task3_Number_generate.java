//2. generáljunk egy tömböt véletlen számokkal, majd írjuk ki azt a számot,
//amikortól kezdve tőle csak nagyobb szám van a tömbben. 
//pl. 
//3, 4, 7, 3, 4, 6 esetén a 3 a megoldás
//9, 3, 1, 5, 2, 4, 6 esetén a 2 a megoldás
package Hazifeladat_8;

//@author Zsolt Horváth
import java.util.Scanner;

public class Task3_Number_generate {

    public static int[] randomNumbers = new int[6];

    public static void main(String[] args) {
        fillArray(1, 10);

        printArray(randomNumbers);
        System.out.println("");
        System.out.println("Ettől a számtól, csak nagyobb van a tömbben: " + getMaxItem());
        System.out.println("");

    }

    public static void printArray(int[] array) {
        System.out.println("Tömb elemei: ");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + ",");
        }
    }

    public static void fillArray(int min, int max) {
        for (int i = 0; i < randomNumbers.length; i++) {
            randomNumbers[i] = (int) (Math.random() * (max - min + 1) + min);
        }
    }

    public static int getMaxItem() {
        int min = randomNumbers[randomNumbers.length - 1];

        for (int i = randomNumbers.length - 1; i >= 0; i--) {
            if (min >= randomNumbers[i]) {
                min = randomNumbers[i];
            }
        }
        return min;

    }

}
