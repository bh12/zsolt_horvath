package zh2_vehicle;
//@author Zsolt Horváth

import java.time.LocalDate;




public abstract class Vehicle implements Comparable<Vehicle> {
    
    private int plateNumber;
    private LocalDate localDate;
    private int weigth;

    public int getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(int plateNumber) {
        this.plateNumber = plateNumber;
    }

    
    
    
    public LocalDate getLocalDate() {
        return localDate;
    }

    public void setLocalDate(LocalDate localDate) {
        this.localDate = localDate;
    }

    public int getWeigth() {
        return weigth;
    }

    public void setWeigth(int weigth) {
        this.weigth = weigth;
    }


    @Override
    public int compareTo(Vehicle o1) {
        if (this.getLocalDate().isAfter(o1.getLocalDate())) {
            return 1;
        }
        if (this.getLocalDate().isBefore(o1.getLocalDate())) {
            return -1;
        } else {
            return 0;
        }
    }

    @Override
    public String toString() {
        return "Vehicle{" + "plateNumber=" + plateNumber + ", localDate=" + localDate + ", weigth=" + weigth + '}';
    }

   

}
