package task2_clock;

//@author Zsolt Horváth
public class Clock {

    private int hours;
    private int minutes;
    private int seconds;

    public Clock(int hours, int minutes, int seconds) {
        if (hours > 23 || hours < 0) {
            System.out.println("Az óra 0-23 érték között adható meg!");
            hours = -1;
        }
        this.hours = hours;

        if (minutes > 59 || minutes < 0) {
            System.out.println("A perc 0-59 érték közöt adható meg!");
            minutes = -1;
        }
        this.minutes = minutes;

        if (seconds > 59 || seconds < 0) {
            System.out.println("A másodperc 0-59 érték közöt adható meg!");
            seconds = -1;
        }
        this.seconds = seconds;
    }

    public String toString() {
        return "[class: A jelenlegi idő: " + hours + ":" + minutes + ":" + seconds + "]";
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int plusMinute) {
        boolean moreThenOneHour = minutes + plusMinute > 59;
        int addMinute = (minutes + plusMinute) - 60;

        if (moreThenOneHour) {
            hours += 1;
            minutes = addMinute;
        } else {
            minutes += plusMinute;
        }
        this.minutes = minutes;
    }

    public int getSeconds() {
        return seconds;
    }

    public void setSeconds(int seconds) {
        this.seconds = seconds;
    }
}
