package hu.braininghub.zh_threadhandling;

/**
 *
 * @author Zsolt
 */
public class MyObject implements Runnable {

    public static void main(String[] args) {
        for (int i = 1; i < 6; i++) {
            MyObject m = new MyObject();
            Thread thread = new Thread(m);

            thread.start();
            
        }
    }

    @Override
    public void run() {
        int rand = (int) (Math.random() * 10) + 10;
        for (int i = 0; i < rand; i++) {
            System.out.println(i + ". Zsolt");
        }

    }

}
