package zh2_swing_gyakorlo.controller;
//@author Zsolt Horváth

import java.io.BufferedReader;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.time.LocalTime;
import java.util.ArrayList;

import zh2_swing_gyakorlo.model.Boss;
import zh2_swing_gyakorlo.model.Model;
import zh2_swing_gyakorlo.model.Office;
import zh2_swing_gyakorlo.model.Worker;

import zh2_swing_gyakorlo.view.View;

public class Controller {

    private LocalTime localTime;
    private View view;
    private Model model;

    public Controller() {
        this.view = new View(this);
        this.model = new Model();
        view.init();
    }

    public void handleCreatWorkorButton() {
        model.creatAWorker();
        logToArea(Worker.class);
    }

    public void handleCreatBossButton() {
        model.creatABoss();
        logToArea(Boss.class);
    }

    public void logToArea(Class<? extends Office> clazz) {
        localTime = LocalTime.now();
        view.getText().append(localTime + "\n" + clazz.toString() + "\n" + model.getOffice().size() + "\n");

    }

    public void serializableObj() {

        try (FileOutputStream fos = new FileOutputStream("Office.ser");
                ObjectOutputStream oos = new ObjectOutputStream(fos)) {
            oos.writeObject(model.getOffice());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void backSerializableObj() {
        
        try (FileInputStream fis = new FileInputStream("Office.ser");
                ObjectInputStream ois = new ObjectInputStream(fis)) {
            model.setOffice((ArrayList) ois.readObject());

        } catch (FileNotFoundException ex) {
            ex.getStackTrace();
        } catch (IOException ex) {
            ex.getStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.getStackTrace();
        }

    }

    public void handleSerializableButton() {
        serializableObj();
       
    }
    public void handleBackSerializableButton() {
        backSerializableObj();
        printBackSerializableList();
       
    }
    
    public void printBackSerializableList(){
        for(Office f : model.getOffice()){
           view.getText().append(f.toString());
        }
    }

}
