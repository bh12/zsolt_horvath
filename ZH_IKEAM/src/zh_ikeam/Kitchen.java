package zh_ikeam;
//@author Zsolt Horváth

import java.time.LocalDate;


public class Kitchen extends Product implements CanTurnOnOff, Movable {

    int turnCounter = 0;

    @Override
    public void setLd(LocalDate ld) {
        super.setLd(ld); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public LocalDate getLd() {
        return super.getLd(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setPrice(int price) {
        super.setPrice(price); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getPrice() {
        return super.getPrice(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setBarCode(int barCode) {
        super.setBarCode(barCode); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getBarCode() {
        return super.getBarCode(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setProducer(String producer) {
        super.setProducer(producer); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getProducer() {
        return super.getProducer(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void turnOn(boolean turn) {

        turnControll();

        if (turn == true) {
            turnCounter++;
        }

    }

    @Override
    public void turnControll() {
        try {
            if (turnCounter == 5) {
                throw new MyExceptionClass("Hiba.");
            }
        } catch (MyExceptionClass ex) {
            ex.toString();
        }

    }

    @Override
    public void move() {
    }

    @Override
    public int getTurnCount() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
