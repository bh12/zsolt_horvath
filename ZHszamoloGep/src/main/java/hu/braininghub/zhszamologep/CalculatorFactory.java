package hu.braininghub.zhszamologep;

/**
 *
 * @author Zsolt
 */
public class CalculatorFactory extends AbstractFactory {

    @Override
    public Calculator getCalculator(String calculatorType) {
        if (calculatorType.equalsIgnoreCase("Old")) {
            return new CalculatorOld();
        } else if (calculatorType.equalsIgnoreCase("Science")) {
            return new CalculatorScience();
        }
        return null;
    }

}
