//2. A program töltsön fel véletlenszerűen kétjegyű számokkal egy 500 elemű tömböt,
//   majd csináljon statisztikát róla, melyik szám hányszor fordul elő!
package hometask5;

//@author Zsolt Horváth
public class Task2_Tomb_Statistic {

    public static void main(String[] args) {

        int[] array = fillArray(500, 10, 99);
        System.out.println("A tömb elemei: ");
        printArray(array);

        int[] statisticsCounter = fillArray(100, 0, 0);
        statistics(array, statisticsCounter);

        System.out.println("A generált számok előfordulása: ");
        printStatArray(statisticsCounter);

    }

    public static int[] fillArray(int size, int min, int max) {
        int[] array = new int[size];
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * (max - min + 1) + min);
        }

        return array;
    }

    public static void printArray(int[] array) {

        for (int i = 0; i < array.length; i++) {
            System.out.println(i + ":" + array[i] + ",");
        }
    }

    public static void statistics(int[] array, int[] statisticsCounter) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] < 100) {
                statisticsCounter[array[i]]++;

            }
        }
    }

    public static void printStatArray(int[] array) {

        for (int i = 10; i < array.length; i++) {
            System.out.println(i + ":" + array[i] + ",");
        }
    }

}
