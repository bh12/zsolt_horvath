package hu.braininghub.dao;

import hu.braininghub.entity.OrderEntity;
import java.util.List;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author User
 */
@Singleton
@TransactionAttribute
public class OrderDAO {

    private static final String QUERY_FIND_BY_FILTER = "select o from OrderEntity o ";

    @PersistenceContext
    private EntityManager em;

    public List<OrderEntity> getAllOrders() {
        TypedQuery<OrderEntity> namedQuery = em.createNamedQuery(OrderEntity.QUERY_FIND_ALL, OrderEntity.class);
        return namedQuery.getResultList();

    }
}
