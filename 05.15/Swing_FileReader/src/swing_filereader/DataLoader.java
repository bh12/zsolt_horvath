package swing_filereader;
//@author Zsolt Horváth

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DataLoader {

    private List<String> dataList = new ArrayList<>();

    public List<String> getDataList() {
        return dataList;
    }

    public List<String> loadDataFromFile() {
        File f = new File("swing.txt");

        try (FileReader fr = new FileReader(f);
                BufferedReader br = new BufferedReader(fr);) {

            while (br.ready()) {
                dataList.add(br.readLine());

            }
        } catch (IOException e) {
            System.out.println("Nem létező file!");
        }
        return dataList;
    }
}
