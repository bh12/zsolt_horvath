<%-- 
    Document   : Car
    Created on : Sep 20, 2020, 3:43:52 PM
    Author     : Zsolt
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Legkevesebbet futott autó</title>

    </head>

    <body>
        <div class="container">


            <div class="row">
                <table class="table table-striped">
                    <thead>
                        <tr>

                            <th>PlateNumber</th>
                            <th>Brand</th>
                            <th>MileAge</th>
                        </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${car}" var="car">
                        <tr>

                            <td>${car.plateNumber}</td>
                            <td>${car.brand}</td>
                            <td>${car.mileage}</td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </body>
</html>
