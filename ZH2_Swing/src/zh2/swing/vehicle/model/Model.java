package zh2.swing.vehicle.model;
//@author Zsolt Horváth

import java.util.ArrayList;
import java.util.List;

public class Model {

    private List<Vehicle> vehicle = new ArrayList<>();
    private Bmw bmw;
    private Airplane airplane;
    
    public void creatBMW() {
        bmw = new Bmw();
        vehicle.add(bmw);

    }

    public void creatAirplane() {
        airplane = new Airplane();
        vehicle.add(airplane);

    }

    public List<Vehicle> getVehicle() {
        return vehicle;
    }

    public void setVehicle(List<Vehicle> vehicle) {
        this.vehicle = vehicle;
    }

    public Bmw getBmw() {
        return bmw;
    }

    public void setBmw(Bmw bmw) {
        this.bmw = bmw;
    }

    public Airplane getAirplane() {
        return airplane;
    }

    public void setAirplane(Airplane airplane) {
        this.airplane = airplane;
    }

}
