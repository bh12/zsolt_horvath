package zh_ikeam;
//@author Zsolt Horváth

import java.time.LocalDate;

public abstract class Product implements Comparable<Product> {
    private String producer;
    private int barCode;
    private int price;
    private LocalDate ld;
    private Attribution at;

    public Attribution getAt() {
        return at;
    }

    public void setAt(Attribution at) {
        this.at = at;
    }
    

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public int getBarCode() {
        return barCode;
    }

    public void setBarCode(int barCode) {
        this.barCode = barCode;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public LocalDate getLd() {
        return ld;
    }

    public void setLd(LocalDate ld) {
        this.ld = ld;
    }

    @Override
    public int compareTo(Product p) {
       if (this.getBarCode() > p.getBarCode()) {
            return 1;
        }
        if (this.getBarCode() < p.getBarCode()) {
            return 0;
        }
        return -1;
    }

}
