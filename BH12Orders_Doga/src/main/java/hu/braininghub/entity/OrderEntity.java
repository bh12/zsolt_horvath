package hu.braininghub.entity;

import static hu.braininghub.entity.OrderEntity.QUERY_FIND_ALL;
import java.time.LocalDate;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author User
 */
@Getter
@Setter
@Entity
<<<<<<< HEAD
@Table(name = "orders")
=======
@Table(name = "order")
>>>>>>> a69b002af21da394b610919a5a7621fca0329841
@NamedQuery(name = QUERY_FIND_ALL, query = "select o from OrderEntity o")
public class OrderEntity extends BaseEntity {

    public static final String QUERY_FIND_ALL = "OrderEntity.findAll";

    @Column
    private LocalDate orderDate;

    @Column
    private LocalDate shippingDate;

    @OneToMany(mappedBy = "order")
    private List<ProductEntity> products;
}