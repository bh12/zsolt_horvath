package swing_filereader;
//@author Zsolt Horváth

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.HeadlessException;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class CustomSwing extends JFrame {

    DataLoader dl = new DataLoader();
    private JLabel label;

    public CustomSwing(String title) throws HeadlessException {
        super(title);
        this.label = label;
    }

    public void init() {
        setVisible(true);
        setSize(500, 500);
        labelMaker();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new FlowLayout(FlowLayout.CENTER));
    }

    public void labelMaker() {
        dl.loadDataFromFile();
        for (int i = 0; i < dl.getDataList().size(); i++) {
            label = new JLabel(dl.getDataList().get(i));

            switch (i) {
                case 0:
                    label.setForeground(Color.BLUE);
                    break;
                case 1:
                    label.setForeground(Color.GREEN);
                    break;
                case 2:
                    label.setForeground(Color.PINK);
                    break;
                default:
                    label.setForeground(Color.BLACK);

            }
            add(label);

        }
    }
}
