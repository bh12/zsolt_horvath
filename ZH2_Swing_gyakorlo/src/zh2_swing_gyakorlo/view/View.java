package zh2_swing_gyakorlo.view;
//@author Zsolt Horváth

import java.awt.Font;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import zh2_swing_gyakorlo.controller.Controller;

public class View extends JFrame {

    Controller controller;

    private JPanel panel;
    private JTextArea text;
    private JButton creatWorker;
    private JButton creatBoss;
    private JButton serializable;
    private JButton load;
    private JScrollPane scroll;

    public View(Controller controller) {
        this.controller = controller;
    }

    public void init() {
        setUpWindow();
        setUpCreatWorkerButton();
        setUpCreatBossButton();
        setUpReaderButton();
        setUpSerializableButton();
        showWindow();
    }

    private void setUpWindow() {
        this.setSize(500, 500);
        this.setLocationRelativeTo(null);
        this.setTitle("LookingForWorkerAndBoss");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(new GridLayout(2, 1));
        setUpTextArea();
        this.panel = new JPanel();
        add(this.panel);

    }

    private void showWindow() {
        this.setVisible(true);
    }

    private void setUpTextArea() {
        this.text = new JTextArea(10, 20);
        this.text.setLineWrap(true);
        Font f = new Font("Arial", Font.BOLD, 20);
        this.text.setFont(f);
        this.scroll = new JScrollPane(this.text);
        add(this.scroll);
    }

    private void setUpCreatWorkerButton() {
        this.creatWorker = new JButton("New Worker");
        this.creatWorker.addActionListener(event -> {
            controller.handleCreatWorkorButton();
        });

        panel.add(this.creatWorker);
    }

    private void setUpCreatBossButton() {
        this.creatBoss = new JButton("New Boss");
        this.creatBoss.addActionListener(event -> {
            controller.handleCreatBossButton();
        });

        panel.add(this.creatBoss);
    }

    private void setUpReaderButton() {
        this.load = new JButton("Szerializált lista betöltése");
        this.load.addActionListener(event -> {
            controller.handleBackSerializableButton();
        });

        panel.add(this.load);

    }

    private void setUpSerializableButton() {
        this.serializable = new JButton("Serializálás");
        this.serializable.addActionListener(event -> {
            controller.handleSerializableButton();
        });

        panel.add(this.serializable);

    }

    public JTextArea getText() {
        return text;
    }

    public void setScreenText(String text) {
        this.text.setText(text);

    }

}
