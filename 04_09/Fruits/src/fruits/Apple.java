package fruits;

//@author Zsolt Horváth
public class Apple extends Fruit {

    protected String taste;

    public Apple() {
    }

    public String getTaste() {
        return taste;
    }

    public void setTaste(String taste) {
        this.taste = taste;
    }

    @Override
    public String toString() {
        return "Apple{ A színe: " + super.getColor() + " és a kg ára: " + super.getPrice() + " Ft." + "Az íze: " + getTaste() + '}';
    }

}
