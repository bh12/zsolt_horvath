//4. A program legyen képes ötös és hatos lottó egy lehetséges húzásának eredményét visszaadni.
//            A program köszöntse a felhasználót, majd egy menüvel döntse el,
//            hogy ötös vagy hatos lottó számokat adjon vissza (menü) 
package hometask5;

//@author Zsolt Horváth
import java.util.Scanner;

public class Task4_Lotto_Menu {

    public static final int EXIT_MENU_NUMBER = 3;

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        int number;
        do {
            System.out.println("\t");
            System.out.println("Hello!");
            System.out.println("Melyik lottoval szeretne játszani? ");

            System.out.println("1. 5ös lotto játék ");
            System.out.println("2. 6os lotto játék ");
            System.out.println(EXIT_MENU_NUMBER + ". Kilépés ");

            number = sc.nextInt();
            switch (number) {
                case 1:
                    System.out.println("Az 5ös lotto számai:");
                    printLotto(fillLotto(5, 1, 90));
                    break;
                case 2:
                    System.out.println("A 6os lotto számai:");
                    printLotto(fillLotto(6, 1, 45));
                    break;
                case 3:
                    System.out.println("Köszönjük a játékot.Viszlát!");
            }
        } while (number != EXIT_MENU_NUMBER);

    }

    public static void sayHello() {
        System.out.println("Hello");
    }

    public static void printLotto(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + ",");
        }
        System.out.println("");
    }

    public static int[] fillLotto(int size, int min, int max) {
        int[] array = new int[size];

        for (int i = 0; i < array.length; i++) {
            int randomNumber = (int) (Math.random() * (max - min + 1) + min);

            boolean usedNumber = false;

            for (int j = 0; j < array.length; j++) {
                if (randomNumber == array[j]) {
                    usedNumber = true;
                }
            }
            if (!usedNumber) {
                array[i] = randomNumber;
            } else {
                i--;
            }
        }
        return array;
    }

}
