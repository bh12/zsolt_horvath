
package daos;
//@author Zsolt Horváth
public class BaseDao {

    private final String CONNECTION_URL = "jdbc:mysql://localhost/bh12_car?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";

    public String getCONNECTION_URL() {
        return CONNECTION_URL;
    }
}
