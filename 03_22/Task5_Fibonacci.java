//5. fibonacci sorozat n. elemének kiszámolására, oldja meg hagyományos és rekurzív módon
package hometask5;

//@author Zsolt Horváth
public class Task5_Fibonacci {

    public static void main(String[] args) {
//        int n1 = 0;
//        int n2 = 1;
//        int n3;
//        
//        System.out.print(n1+" "+n2);
//
//        for (int i  = 0; i < 20; ++i){
//            n3 = n1 + n2;
//            System.out.print("," + n3);
//            n1 = n2;
//            n2 = n3;
//        }

        System.out.println(fibonacci(3));
        System.out.println(fibonacci(4));
        System.out.println(fibonacci(5));
        System.out.println(fibonacci(6));
        System.out.println(fibonacci(7));
        System.out.println(fibonacci(8));
        
        
    }

    public static long fibonacci(long n) {
       
        if ((n == 0) || (n == 1)) {
            return n;
        } else {
            return fibonacci(n - 1) + fibonacci(n - 2);
        }
    }

}
