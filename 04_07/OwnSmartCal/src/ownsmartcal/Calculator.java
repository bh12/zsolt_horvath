package ownsmartcal;

//@author Zsolt Horváth
import java.util.Scanner;

public class Calculator {

    Scanner sc = new Scanner(System.in);

    private int number1;
    private int number2;
    private char op;

    private double result;

    public double askOperator() {
        do {
            number1 = askANumber();
            System.out.println("Kérlek, adj meg egy műveletet: +,-,=,x");
            op = sc.next().charAt(0);
            switch (op) {
                case '+':
                    this.number2 = askANumber();
                    result = number1+this.number2;
                    System.out.println("Eredmény: " + result);
                    return askOperator();

                case '-':
                    this.number2 = askANumber();
                    result = number1-this.number2;
                    System.out.println("Eredmény: " + result);
                    return askOperator();
                case '=':
                    System.out.println("Eredmény: " + result);
                    break;

            }
        } while (op != 'x');
        return result;
    }

    public int askANumber() {
        System.out.println("Kérlek, adj meg egy számot: ");
        return this.number1 = sc.nextInt();

    }

    public double start() {
        return askOperator();
    }
}
