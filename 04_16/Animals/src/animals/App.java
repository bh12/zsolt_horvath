/*
 Legyen egy kert, ahol vannak állatok, 100 db, csirke, macska stb.
 Legyenek vadállatok, pl. farkas
 minden állatnak legyen hp-je, vadállatoknak damage

 vadállatok betörnek a kertbe, próbálnak legyőzni háziállatokat
 */

package animals;

import java.util.Random;

public class App {

    public static final Random rnd = new Random();

    public static void main(String[] args) {
        Creature[] creatures = new Creature[100];
        generateCreatures(creatures);

        //elso farkas
        Wolf attackerWolf = generateAttackerAnimal(creatures);
        //elso nem farkas
        Animal poorAnimal = generateDeffensiveAnimal(creatures);

        int animalCounter = 0;
        while (attackerWolf != null || poorAnimal != null) {

            animalCounter++;

//            if (attackerWolf == null || poorAnimal == null) {
//                System.out.println("counter: " + animalCounter);
//                break;
//            }
            //megtamadott allat mutatasa
            System.out.println(poorAnimal);
            //a farkas harap
            attackerWolf.attack();
            //elso harapas
            poorAnimal.setLiveScore(poorAnimal.getLiveScore() - attackerWolf.getAttackScore());
            //visszaharapasra valo lehetoseg ellenorzese
            if (poorAnimal instanceof AttackInterface) {//megnezem, hogy az adott allat implementalja-e az attack interfacet
                // a haboru
                theWar(poorAnimal, attackerWolf);
            }

            //kovetkezo farkas, ha meghalt az elozo
            attackerWolf = generateAttackerAnimal(creatures);
            //kovetkezo allat
            poorAnimal = generateDeffensiveAnimal(creatures);
        }
        System.out.println(animalCounter);
    }

    public static void generateCreatures(Creature[] creatures) {
        for (int i = 0; i < creatures.length; i++) {
            int rndNumber = rnd.nextInt(100);
            if (rndNumber < 60) {
                creatures[i] = new Chicken(rnd.nextInt(5));
            } else if (rndNumber < 65) {
                creatures[i] = new Turkey(rnd.nextInt(5));
            } else if (rndNumber < 70) {
                creatures[i] = new Cat(rnd.nextInt(10));
            } else if (rndNumber < 80) {
                creatures[i] = new Dog(rnd.nextInt(20));
            } else {
                creatures[i] = new Wolf(rnd.nextInt(20));
            }
        }

    }

    public static Wolf generateAttackerAnimal(Creature[] creatures) {
        Wolf attackerWolf = null;
        for (int i = 0; i < creatures.length; i++) {
            if (creatures[i] instanceof Wolf && creatures[i].isLive()) {
                attackerWolf = (Wolf) creatures[i];
                return attackerWolf;

            }
        }
        return null;
    }

    public static Animal generateDeffensiveAnimal(Creature[] creatures) {
        Animal poorAnimal = null;
        for (int i = 0; i < creatures.length; i++) {
            if (!(creatures[i] instanceof Wolf) && creatures[i].isLive()) {
                poorAnimal = (Animal) creatures[i];
                return poorAnimal;

            }
        }
        return null;
    }

    public static void theWar(Animal poorAnimal, Wolf attackerWolf) {
        AttackInterface attackerPoorAnimal = (AttackInterface) poorAnimal;
        boolean bothAlive = true;
        while (bothAlive) {
            attackerPoorAnimal.attack();
            attackerWolf.setLiveScore(attackerWolf.getLiveScore() - attackerPoorAnimal.getAttackScore());

            if (attackerWolf.isLive()) {
                attackerWolf.attack();
                poorAnimal.setLiveScore(poorAnimal.getLiveScore() - attackerWolf.getAttackScore());
            }
            bothAlive = attackerWolf.isLive() && poorAnimal.isLive();
        }

    }
}