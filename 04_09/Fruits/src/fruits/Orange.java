package fruits;

//@author Zsolt Horváth
public class Orange extends Fruit {

    public Orange() {
    }

    @Override
    public String getColor() {
        return super.color = "Nancssárga";
    }

    @Override
    public String toString() {
        return "Orange{ A színe: " +getColor()+" és a kg ára: "+super.getPrice()+" Ft."+'}';
    }

}
