package hu.braininghub.zh_car.dao;

import hu.braininghub.zh_car.dto.CarDTO;
import hu.braininghub.zh_car.entity.CarEntity;
import hu.braininghub.zh_car.mapper.CarMapper;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Zsolt
 */
@Stateless
public class CarDAO {

    @PersistenceContext
    EntityManager em;

    private static final String QUERY_GET_ALL_CAR = "SELECT c FROM CarEntity c";
//    private static final String QUERY_LOWEST_MILEAGE_CAR = "SELECT c FROM CarEntity c WHERE c.mileage = (SELECT MIN(c.mileage) FROM CarEntity c)";

    public void addNewCar(String plateNumber, String brand, int mileage) {
        CarEntity car = new CarEntity();
        car.setBrand(brand);
        car.setPlateNumber(plateNumber);
        car.setMileage(mileage);
        em.persist(car);
    }

    public List<CarDTO> getCars() {
        List<CarEntity> cars = em.createQuery(QUERY_GET_ALL_CAR).getResultList();
        List<CarDTO> carDTOs = new ArrayList();
        for (CarEntity car : cars) {
            carDTOs.add(CarMapper.INSTANCE.toDTO(car));
        }

        return carDTOs;
    }

    public List<CarDTO> getLowestMileAgeCar() {
        List<CarDTO> cars = getCars();

        CarDTO minByMileage = cars
                .stream()
                .min(Comparator.comparingInt(CarDTO::getMileage))
                .get();
        List<CarDTO> carss = new ArrayList<>();
        carss.add(minByMileage);
        return carss;
    }
}
