package swingserializable;
//@author Zsolt Horváth

import java.io.IOException;
import java.io.ObjectInputStream;

public class InputStream<T> extends ObjectInputStream {

  public InputStream(java.io.InputStream in) throws IOException {
        super(in);
    }

    public InputStream() throws IOException, SecurityException {
    }

    public T readObjectGeneric() throws IOException, ClassNotFoundException {
        return (T) readObject();
    }

}
