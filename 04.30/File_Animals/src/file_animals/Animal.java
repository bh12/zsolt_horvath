package file_animals;

//@author Zsolt Horváth
public class Animal {

    private String name;
    private String isHair;
    private String isFeathers;
    private String isEggs;
    private String isMilk;
    private String isAirborne;
    private String isAquatic;
    private String isPredator;
    private String isToothed;
    private String isBackbone;
    private String isBreathes;
    private String isVenomous;
    private String isFins;
    private String Legs;
    private String isTail;
    private String isDomestic;
    private String isCatsize;
    private String isClass_type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIsHair() {
        return isHair;
    }

    public void setIsHair(String isHair) {
        this.isHair = isHair;
    }

    public String getIsFeathers() {
        return isFeathers;
    }

    public void setIsFeathers(String isFeathers) {
        this.isFeathers = isFeathers;
    }

    public String getIsEggs() {
        return isEggs;
    }

    public void setIsEggs(String isEggs) {
        this.isEggs = isEggs;
    }

    public String getIsMilk() {
        return isMilk;
    }

    public void setIsMilk(String isMilk) {
        this.isMilk = isMilk;
    }

    public String getIsAirborne() {
        return isAirborne;
    }

    public void setIsAirborne(String isAirborne) {
        this.isAirborne = isAirborne;
    }

    public String getIsAquatic() {
        return isAquatic;
    }

    public void setIsAquatic(String isAquatic) {
        this.isAquatic = isAquatic;
    }

    public String getIsPredator() {
        return isPredator;
    }

    public void setIsPredator(String isPredator) {
        this.isPredator = isPredator;
    }

    public String getIsToothed() {
        return isToothed;
    }

    public void setIsToothed(String isToothed) {
        this.isToothed = isToothed;
    }

    public String getIsBackbone() {
        return isBackbone;
    }

    public void setIsBackbone(String isBackbone) {
        this.isBackbone = isBackbone;
    }

    public String getIsBreathes() {
        return isBreathes;
    }

    public void setIsBreathes(String isBreathes) {
        this.isBreathes = isBreathes;
    }

    public String getIsVenomous() {
        return isVenomous;
    }

    public void setIsVenomous(String isVenomous) {
        this.isVenomous = isVenomous;
    }

    public String getIsFins() {
        return isFins;
    }

    public void setIsFins(String isFins) {
        this.isFins = isFins;
    }

    public String getLegs() {
        return Legs;
    }

    public void setLegs(String Legs) {
        this.Legs = Legs;
    }

    public String getIsTail() {
        return isTail;
    }

    public void setIsTail(String isTail) {
        this.isTail = isTail;
    }

    public String getIsDomestic() {
        return isDomestic;
    }

    public void setIsDomestic(String isDomestic) {
        this.isDomestic = isDomestic;
    }

    public String getIsCatsize() {
        return isCatsize;
    }

    public void setIsCatsize(String isCatsize) {
        this.isCatsize = isCatsize;
    }

    public String getIsClass_type() {
        return isClass_type;
    }

    public void setIsClass_type(String isClass_type) {
        this.isClass_type = isClass_type;
    }

    @Override
    public String toString() {
        return "Animal{" + "name=" + name + ", isHair=" + isHair + ", isFeathers=" + isFeathers + ", isEggs=" + isEggs + ", isMilk=" + isMilk + ", isAirborne=" + isAirborne + ", isAquatic=" + isAquatic + ", isPredator=" + isPredator + ", isToothed=" + isToothed + ", isBackbone=" + isBackbone + ", isBreathes=" + isBreathes + ", isVenomous=" + isVenomous + ", isFins=" + isFins + ", Legs=" + Legs + ", isTail=" + isTail + ", isDomestic=" + isDomestic + ", isCatsize=" + isCatsize + ", isClass_type=" + isClass_type + '}';
    }

}