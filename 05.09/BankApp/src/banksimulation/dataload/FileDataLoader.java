package banksimulation.dataload;

import banksimulation.BankApplication;
import banksimulation.model.Client;
import banksimulation.model.Bank;
import banksimulation.model.Transfer;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class FileDataLoader implements DataLoader {

    @Override
    public void loadInitialDataClient() {
        File clientFile = new File("clients.txt");

        try (FileReader fr = new FileReader(clientFile);
                BufferedReader br = new BufferedReader(fr);) {
            String line = br.readLine();
            while (line != null) {
                String[] clientArray = line.split(",");

                String clientId = clientArray[0];
                String accountNumber = clientArray[1];
                long balance = Long.parseLong(clientArray[2]);

                Client c = new Client(clientId, accountNumber, balance);

                BankApplication.clients.add(c);

                line = br.readLine();
            }
        } catch (IOException e) {
            System.out.println("Hiba a fájl beolvasása közben! :(");
        }
    }

    @Override
    public void loadInitialDataBank() {
        File bankFile = new File("banks.txt");

        try (FileReader fr = new FileReader(bankFile);
                BufferedReader br = new BufferedReader(fr);) {
            String line = br.readLine();
            while (line != null) {
                String[] bankArray = line.split(",");
                String bankName = bankArray[0];

                Bank b = new Bank(bankName);
                BankApplication.banks.add(b);
                line = br.readLine();
            }
        } catch (IOException e) {
            System.out.println("Hiba a fájl beolvasása közben!");
        }
    }

    @Override
    public void loadInitialDataTransfer() {
        File clientFile = new File("transfers.txt");

        try (FileReader fr = new FileReader(clientFile);
                BufferedReader br = new BufferedReader(fr);) {
            String line = br.readLine();
            while (line != null) {
                String[] clientArray = line.split(",");
                String lookingForSource = clientArray[0];
                List<String> clientSource = BankApplication.clients.stream()
                        .map(client -> client.getClientId())
                        .collect(Collectors.toList());
                int lookingForIndex = clientSource.indexOf(lookingForSource);
                Client source = BankApplication.clients.get(lookingForIndex);

                String lookingForTarget = clientArray[1];
                int lookingForTargetIndex = clientSource.indexOf(lookingForTarget);
                Client target = BankApplication.clients.get(lookingForTargetIndex);

                long amount = Long.parseLong(clientArray[2]);
                Date date = new Date();
                Transfer t = new Transfer(source, target, amount, date);
                BankApplication.transfers.add(t);

                line = br.readLine();
            }
        } catch (IOException e) {
            System.out.println("Hiba a fájl beolvasása közben! :(");
        }
    }

}
