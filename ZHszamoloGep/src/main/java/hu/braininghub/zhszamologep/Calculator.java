package hu.braininghub.zhszamologep;

/**
 *
 * @author Zsolt
 */
public abstract class Calculator {

    public static double result = 0;

    public double getSum(double a) {
        result = result + a;
        return result;
    }

    public double getSub(double a) {
        result = result - a;
        return result;
    }

    public double getMulti(double a) {
        result = result * a;
        return result;
    }

    public double getDiv(double a) {
        result = result / a;
        return result;
    }
}
