package hu.braininghub.entity;

import static hu.braininghub.entity.ProductEntity.QUERY_FIND_ALL;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author User
 */
@Entity
@Getter
@Setter
@Table(name = "product")
@NamedQuery(name = QUERY_FIND_ALL, query = "select p from ProductEntity p ")
public class ProductEntity extends BaseEntity {

    public static final String QUERY_FIND_ALL = "ProductEntity.findAll";

    @Column
    private String name;

    @Column
    private Integer price;

    @Column
    private String description;

    @ManyToOne
    @JoinColumn(name = "order_id")
    private OrderEntity order;

}
