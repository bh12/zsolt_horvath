<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Új autó hozzáadása</title>

    </head>

   <body>
        <div class="container">

            <div class="alert alert-danger" id="errorBox" style="display: none;">
                <strong>Nem valid a form!</strong>
            </div>
            <form method="POST" action="addCarServlet">
                <div class="form-group">
                    <label for="plateNumber">Rendszám:</label>
                    <input type="text" class="form-control" placeholder="" id="plateNumber" name="plateNumber">
                </div>
                <div class="form-group">
                    <label for="brand">Márka:</label>
                    <input type="text" class="form-control" placeholder="" name="brand" id="brand">
                </div>
                <div class="form-group">
                    <label for="mileage">KM óra állás:</label>
                    <input type="text" class="form-control" placeholder="" name="mileage" id="mileage">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </body>

</html>
