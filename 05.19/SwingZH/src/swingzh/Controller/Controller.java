package swingzh.Controller;
//@author Zsolt Horváth

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import swingzh.model.Model;
import swingzh.view.AppView;

public class Controller {

    private AppView view;
    private Model model;

    public Controller() {
        this.model = new Model();
        this.view = new AppView(this);
        view.init();
    }

    public void handleAddButton() {
        updateViewFromModel();
        model.getList().add(model.getCounter()+"."+view.getText());
        writeToFile();
    }

    public void updateViewFromModel() {
        view.setScreenText(model.getLocalDate());
    }

    public void writeToFile() {
        
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(model.getF()))) {
            for (String str : model.getList()) {

                bw.write(str + System.lineSeparator());
                
                
              
            }
        } catch (IOException ex) {
            System.out.println("Hiba");
        }
     
    }
}
