package animals;

import java.util.Random;

public class BH12Animals {
    public static final Random rnd = new Random();
    
    public static void main1(String[] args) {
        /*Animal a = new Animal("cat", true, true, true, true);
        Animal b = new Animal("cat", true, true, true, true);
        Animal c = a;
        System.out.println(a.getClass());
        if(a == c) {
            System.out.println("igen");
        } else {
            System.out.println("nem");
        }
        if (a.equals(b)) {
            
        }*/
                
        Creature[] creatures = new Creature[100];
        for (int i = 0; i < creatures.length; i++) {
            int rndNumber = rnd.nextInt(100);
            if(rndNumber<60) {
                creatures[i] = new Chicken(rnd.nextInt(5));
            } else if(rndNumber<65) {
                creatures[i] = new Turkey(rnd.nextInt(5));
            } else if(rndNumber<70) {
                creatures[i] = new Cat(rnd.nextInt(10));
            } else if(rndNumber<80) {
                creatures[i] = new Dog(rnd.nextInt(20));
            } else {
                creatures[i] = new Wolf(rnd.nextInt(20));
            }            
        }

        /*
        Bal oldal, statikus típus, Animal
        Jobb oldal, dinamikus típus, Cat
        Statikus típus "szemüvegén" nézed a dinamikus típust, tehát csak azt látod, 
        amit a statikus típus engedélyez
        */
        /*
        Animal a = new Cat();
        Cat cat = (Cat)a;
        cat.purr();
        */

        /*
        for (int i = 0; i < animals.length; i++) {
            animals[i].sayHello();
            if(animals[i] instanceof Cat) {//ha az adott objektum castolhato az adott tipusra
                Cat c = (Cat)animals[i];
                c.purr();              
                
                //((Cat)animals[i]).purr();
            }
        }*/
        
        //Animal a = new Animal("asdf", true, true, true, true);
        
    }
}

/*
Legyen egy kert, ahol vannak állatok, 100 db, csirke, macska stb.
Legyenek vadállatok, pl. farkas
minden állanak legyen hp-je, vadállatoknak damage

vadállatok betörnek a kertbe, próbálnak legyőzni háziállatokat
*/