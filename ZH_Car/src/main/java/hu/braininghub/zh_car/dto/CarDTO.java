package hu.braininghub.zh_car.dto;

/**
 *
 * @author Zsolt
 */
public class CarDTO {

    private Long id;
    private String plateNumber;
    private String brand;
    private Integer mileage;

    public CarDTO(Long id, String plateNumber, String brand, Integer mileage) {
        this.id = id;
        this.plateNumber = plateNumber;
        this.brand = brand;
        this.mileage = mileage;
    }

    public CarDTO() {

    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getMileage() {
        return mileage;
    }

    public void setMileage(Integer mileage) {
        this.mileage = mileage;
    }

}
