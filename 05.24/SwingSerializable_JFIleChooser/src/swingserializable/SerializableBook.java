
package swingserializable;
//@author Zsolt Horváth

import java.io.Serializable;

public class SerializableBook implements Serializable {
    private String content = "Alma hullik a fáról, jaj de jó";
    private String writer;
    private int numberOfPage;
    private int date;

    public SerializableBook() {
    }

    
    public SerializableBook(String writer, int numberOfPage, int date) {
        this.writer = writer;
        this.numberOfPage = numberOfPage;
        this.date = date;
    }

    @Override
    public String toString() {
        return "SerializableBook{" + "content=" + content + ", writer=" + writer + ", numberOfPage=" + numberOfPage + ", date=" + date + '}';
    }
    

    
    
}
