package swingserializable.view;
//@author Zsolt Horváth

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import swingserializable.controller.Controller;

public class View extends JFrame {

    private Controller controller;

    private JPanel panel;
    private JTextArea text;
    private JButton save;
    private JButton load;
    private JButton seralizable;
    private JButton backSeralizable;

    public View(Controller controller) {
        this.controller = controller;
    }

    public void init() {
        setUpWindow();

        setUpTextArea();
        setUpReaderButton();
        setUpSAVEButton();
        setUpSerializableButton();
        setUpBackSerializableButton();
        showWindow();

    }

    private void setUpWindow() {
        this.setSize(500, 500);
        this.setLocationRelativeTo(null);
        this.setTitle("SaveToFileAPP");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        panel = new JPanel();
        panel.setLayout(new GridBagLayout());

        add(panel);
    }

    private void showWindow() {
        this.setVisible(true);
    }

    private void setUpTextArea() {
        this.text = new JTextArea(8, 20);
        Font f = new Font("Arial", Font.BOLD, 20);
        this.text.setFont(f);

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridwidth = 5;

        panel.add(this.text, gbc);
    }

    private void setUpSAVEButton() {
        this.save = new JButton("SAVE JTextArea");
        this.save.addActionListener(event -> {
            controller.handleSaveButton();
        });
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbc.gridx = 4;
        gbc.gridy = 4;

        panel.add(this.save, gbc);
    }

    private void setUpReaderButton() {
        this.load = new JButton("Open file!");
        this.load.addActionListener(event -> {
            controller.loadDataFromFile();
        });
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbc.gridx = 4;
        gbc.gridy = 5;

        panel.add(this.load, gbc);

    }

    private void setUpSerializableButton() {
        this.seralizable = new JButton("Szerializálj!");
        this.seralizable.addActionListener(event -> {
            controller.handleSerializableButton();
        });

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbc.gridx = 4;
        gbc.gridy = 6;

        panel.add(this.seralizable, gbc);
    }

    private void setUpBackSerializableButton() {
        this.backSeralizable = new JButton("Szerializált file beolvasása.");
        this.backSeralizable.addActionListener(event -> {
            controller.handleBackSerializableButton();
        });
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbc.gridx = 4;
        gbc.gridy = 7;

        panel.add(this.backSeralizable, gbc);
    }

    public JTextArea getText() {
        return text;
    }

    public void setScreenText(String text) {
        this.text.setText(text);

    }

}
