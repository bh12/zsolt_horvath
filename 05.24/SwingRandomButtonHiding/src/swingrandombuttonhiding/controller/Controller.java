package swingrandombuttonhiding.controller;
//@author Zsolt Horváth

import swingrandombuttonhiding.view.View;

public class Controller {

    private View view;

    public Controller() {

        this.view = new View(this);
        this.view.init();
    }

    public void handleNumberButtonClick(String str) {
        int clickedButton = Integer.parseInt(str)-1;
        int randomNumber = clickedButton;
        while (randomNumber == clickedButton) {
            randomNumber = (int) (Math.random() * 9);
        }
        for (int i = 0; i < 9; i++) {
            view.getNumberButtons()[i].setVisible(true);
        }
        view.getNumberButtons()[randomNumber].setVisible(false);

    }

}
