package hu.braininghub.dto;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author User
 */
@Getter
@Setter
public class ProductDTO {

    private String name;
    private Long price;
    private String description;

}
