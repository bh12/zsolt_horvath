package hu.braininghub.dto;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author User
 */

public class ListProducts {

    List<ProductDTO> products;
    
    public List<ProductDTO> getProducts() {
        return products;
}

    public void setProducts(List<ProductDTO> products) {
        this.products = products;
    }

}
