// Irjunk egy konyvolvaso Swinges programot ami kepes beolvasni egy szerializalt konyvet,
// vagy egy text file-t.
// Dontsuk el a kiterjesztes alapjan. .ser kiterjesztesu fileokat 
// kezeljunk binaris szerializalt filenak .txt kiterjesztesueket pedig sima szovegfilenak.
// A file tartalmat toltsuk be egy textareaba. Egy save gomb lenyomasakor mentsuk le
// a textarea tartalmat egy szovegfileba.
// Egy serialize gomb megnyomasara szerializaljuk ki a a textarea tartalmat egy fileba
package swingserializable;

import swingserializable.controller.Controller;


//@author Zsolt Horváth

public class Main {

    
    public static void main(String[] args) {
       Controller cl = new Controller();
    }

}
