package threadqueue;
//@author Zsolt Horváth

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;

import java.util.Queue;
import java.util.Scanner;

public class QueueMaker implements Runnable {

    private final Scanner sc = new Scanner(System.in);
    private Queue<String> asd = new LinkedList<>();

    public synchronized Queue<String> addAsd(String line) {
        asd.add(line);
        return asd;
    }

    public synchronized FileWriter writeFromAsd(FileWriter fw) throws IOException {
        fw.write(asd + System.lineSeparator());
        return fw;
    }

    @Override
    public void run() {
        printToFile();
    }

    public Queue<String> uploadQueue() {
        System.out.println("Írj valamit: ");
        String line = sc.nextLine();

        while (!line.equals("")) {
            line = sc.nextLine();
            addAsd(line);
        }
        return asd;
    }

    public void printQueue() {
        System.out.println(asd);
    }

    public void printToFile() {
        File f = new File("queue.txt");
        try (FileWriter fw = new FileWriter(f)) {
            writeFromAsd(fw);
        } catch (IOException e) {
            System.out.println("A lista nem érhető el!");
        }

    }

}
