package car.servlets;
//@author Zsolt Horváth

import daos.BaseDao;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDate;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "BookingServlet", urlPatterns = {"/BookingServlet"})
public class BookingServlet extends HttpServlet {
    BaseDao baseDao = new BaseDao();
    private static final String QUERY_INSERT_BOKKING = "INSERT INTO booking (start_date,end_date) VALUES (?,?)";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        req.getRequestDispatcher("Booking.jsp").forward(req, res);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try (Connection conn = DriverManager.getConnection(baseDao.getCONNECTION_URL(), "admin", "admin");
                PreparedStatement ps = conn.prepareStatement(QUERY_INSERT_BOKKING)) {

            LocalDate startDate = LocalDate.parse(req.getParameter("start_date"));
            LocalDate endDate = LocalDate.parse(req.getParameter("end_date"));

            ps.setDate(1, Date.valueOf(startDate));
            ps.setDate(2, Date.valueOf(endDate));

            System.out.println("Ennyi sor insertalodott: " + ps.executeUpdate());
            
            resp.sendRedirect("MainServlet");  

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
