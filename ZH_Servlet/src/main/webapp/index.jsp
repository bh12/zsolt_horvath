<%-- 
    Document   : index
    Created on : Sep 20, 2020, 2:07:12 PM
    Author     : Zsolt
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <div class="container">

            <form action="MainServlet" method="get">
                <label for="text">First number: </label>
                <input type="text" palceholder="First number" name="firstNumber" >

                <label for="text">Second number: </label>
                <input type="text" palceholder="Second number" name="secondNumber" >

                <button type="submit">Submit</button>

                <%
                    Object result = request.getAttribute("result");
                %>
                <p><%=result%></p>


            </form>
        </div>
    </body>
</html>
