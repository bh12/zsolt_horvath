//1. A program dolgozzon egy 20 elemű tömbbel, töltse fel véletlen kétjegyű számokkal!
//     Minden részfeladatot külön metódusba kell megvalósítani
//    - Listázza a tömbelemeket
//    - Páros tömbelemek összege
//    - Van-e öttel osztható szám
//    - Melyik az első páratlan szám a tömbben
//    - Van-e a tömbben 32
package hometask5;

//@author Zsolt Horváth
public class Task1 {

    public static void main(String[] args) {
        int[] array = new int[20];

        fillArray(array,10,100);
        printArray(array);
        System.out.println("");
        oddArray(array);
        isDevideFive(array);
        firstEvenNumber(array);

        boolean isDivFive = isDevideFive(array);
        System.out.println("");
        System.out.println("Van benne 5-tel osztható szám?");
        System.out.println(isDivFive);

        int evenNumber = firstEvenNumber(array);
        System.out.println("");
        System.out.println("Az első páratlan szám a tömbben: " + evenNumber);

        boolean looking32 = looking32(array);
        System.out.println("");
        System.out.println("Szerepel a 32es szám a tömbben?");
        System.out.println(looking32);
    }

    public static void printArray(int[] array) {
        System.out.println("A tömb elemei: ");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + ",");
        }
    }

    public static int[] fillArray(int[] array , int min , int max) {

        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * (max-min+1) + min);
        }
        return array;
    }

    public static void oddArray(int[] array) {
        int sum = 0;
        for (int i = 0; i < array.length; i++) {

            if (array[i] % 2 == 0) {
                sum += array[i];
            }
        }
        System.out.println("A páros számok összege: "+sum);
    }

    public static boolean isDevideFive(int[] array) {
        boolean isDevideFive = false;

        for (int i = 0; i < array.length; i++) {
            if (array[i] % 5 == 0) {
                isDevideFive = true;
                break;
            } else {
                isDevideFive = false;
            }
        }
        return isDevideFive;
    }

    public static int firstEvenNumber(int[] array) {
        int evenNumber = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] % 2 == 1) {
                evenNumber = array[i];
                break;
            }
        }
        return evenNumber;
    }

    public static boolean looking32(int[] array) {
        boolean lookingNumber = false;

        for (int i = 0; i < array.length; i++) {
            if (array[i] == 32) {
                lookingNumber = true;
                break;
            } else {
                lookingNumber = false;
            }
        }
        return lookingNumber;
    }

}
