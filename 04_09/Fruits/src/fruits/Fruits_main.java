// Legyen egy Fruit osztalyod. Minden Fruitnak van valamilyen szine es kilogrammonkenti ara.
// Ebbol szarmaztass le egy Banana, Apple, Orange osztalyt.
// A Banana osztaly specialis, mert annak mindig a dekagrammonkenti arat szeretnenk megtudni.
// Az Applenek van iz tipusa is. Pl lehet fanyar, edes, stb.. egy Apple.
// Az Orange mindig naranccsarga szinu.
package fruits;

//@author Zsolt Horváth
public class Fruits_main {

    public static void main(String[] args) {  
//        Fruit banán = new Banana();
//        banán.setColor("Sárga");
//        banán.setPrice(600);
//        System.out.println(banán);
//        
//        Fruit narancs = new Orange();
//        narancs.setPrice(350);
//        System.out.println(narancs);
          
        Banana banán = new Banana();
        banán.setColor("Sárga");
        banán.setPrice(550);
        System.out.println(banán);
        
        Orange narancs = new Orange();
        narancs.setColor("LILA");
        narancs.setPrice(350);
        System.out.println(narancs);
        
        
        Apple alma = new Apple();
        alma.setColor("Piros");
        alma.setPrice(220);
        alma.setTaste("Fanyar");

        System.out.println(alma);
      
    }

}
