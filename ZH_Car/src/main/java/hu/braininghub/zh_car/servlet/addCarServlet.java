/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.zh_car.servlet;

import hu.braininghub.zh_car.service.CarService;
import java.io.IOException;
import java.io.PrintWriter;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Zsolt
 */
@WebServlet(name = "addCarServlet", urlPatterns = {"/addCarServlet"})
public class addCarServlet extends HttpServlet {

    @Inject
    CarService carService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("index.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String plateNumber = req.getParameter("plateNumber");
        String brand = req.getParameter("brand");
        Integer mileage = Integer.parseInt(req.getParameter("mileage"));
        carService.addNewCar(plateNumber, brand, mileage);

        resp.sendRedirect(req.getContextPath() + "/index.jsp");
    }

}
