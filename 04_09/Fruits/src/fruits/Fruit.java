package fruits;

//@author Zsolt Horváth
public class Fruit {

    protected String color;
    protected double price;

    public Fruit() {
    }

    public Fruit(String color, double price) {
        this.color = color;
        this.price = price;
    }

    public String toString() {
        return "Fruit{" + "color=" + this.color + ", price=" + this.price + '}';
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

}
