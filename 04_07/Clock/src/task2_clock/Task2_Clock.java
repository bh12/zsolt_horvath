package task2_clock;

//@author Zsolt Horváth
public class Task2_Clock {

    public static void main(String[] args) {
        Clock clock = new Clock(13, 30, 12);

        System.out.println(clock.toString());
        clock.setMinutes(30);
        System.out.println(clock.toString());
        clock.setMinutes(50);
        System.out.println(clock.toString());
    }
}
