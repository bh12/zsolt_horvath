package animals;

public class Cat extends Animal {
    public Cat(int age) {
        super("cat", true, false, true, false);
        setAge(age);
        setLive(true);
        setLiveScore(50);
    }

    @Override
    public void sayHello() { 
        System.out.println("miau-miau");
    }
    
    public void purr() {
        System.out.println("purr");
    }

    @Override
    public String getDeathScream() {
        return "még van 8, ne aggódj \n";
    }
}
