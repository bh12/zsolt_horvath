package animals;


public class Dog extends Animal implements AttackInterface {
    public static boolean underAttack = false;

    public Dog(int age) {
        super("dog", true, false, true, true);
        setAge(age);
        setLive(true);
        setLiveScore(250);
    }

    @Override
    public void sayHello() {
        System.out.println("vau-vau");
    }    

    @Override
    public String getDeathScream() {
        return "nagyihoz költöztem és a kertben rohangálok \n";
    }    

    @Override
    public int getAttackScore() {
        if(getAge()<=1) {
            return 0;
        } else if (getAge()>9) {
            return 10;
        }
        
        return 15;
    }
    
    @Override
    public void attack() {
        System.out.println("\t\t\t Dog's bite !  +  Dog's livescore: " + getLiveScore());
        underAttack = true;
    }
}
