// Írjon programot, ami egy szálon String sorokat kér be (ez akár lehet a „main” szál is) és
// beteszi az adatot egy Queue-ba, és egy másik szálon pedig ezt a Queue-t felhasználva kiírja
// egy fájlba.
// Ne feledkezzünk a synchronized-ról!
package threadqueue;

//@author Zsolt Horváth
public class ThreadQueue {

    public static void main(String[] args) throws InterruptedException {
        QueueMaker qu = new QueueMaker();
        Thread threadOne = new Thread(qu);
        qu.uploadQueue();
        threadOne.start();


        qu.printQueue();

    }

}
