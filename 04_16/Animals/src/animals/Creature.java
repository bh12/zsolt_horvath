package animals;

public abstract class Creature {
    private boolean live;
    private int age;
    private int liveScore;
    
    public abstract String getDeathScream();

    public boolean isLive() {
        return live;
    }

    public void setLive(boolean live) {
        this.live = live;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getLiveScore() {
        return liveScore;
    }

    public void setLiveScore(int liveScore) {
        if(liveScore<=0) {
            this.live = false;
            this.liveScore = 0;
            System.out.println(this.getDeathScream());//halalhorges
        } else {
            this.liveScore = liveScore;
        }        
    }
}
