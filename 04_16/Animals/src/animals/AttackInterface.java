package animals;

public interface AttackInterface {
    //Konstansok
    int MAX_DAMAGE = 100; //public static final int MAX_DAMAGE = 100;

    //Abstract metodusok
    int getAttackScore();
    public abstract void attack();
    
    //static method: kesobb
    //default method: kesobb
    //kepes orokolni masik interfacebol
}
