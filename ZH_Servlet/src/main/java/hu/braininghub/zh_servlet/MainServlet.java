package hu.braininghub.zh_servlet;

import hu.braininghub.zh_servlet.exception.CustomException;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Zsolt
 */
@WebServlet(name = "MainServlet", urlPatterns = {"/MainServlet"})
public class MainServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Integer firstNumber = 0;
        Integer secondNumber = 0;
        try {
            firstNumber = Integer.parseInt(request.getParameter("firstNumber"));
            secondNumber = Integer.parseInt(request.getParameter("secondNumber"));
        } catch (NumberFormatException ex) {
            throw new CustomException("Nem számot adtál meg.");
        }

        Integer result = firstNumber + secondNumber;
        request.setAttribute("result", result);
        request.getRequestDispatcher("index.jsp").forward(request, response);
        
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

}
