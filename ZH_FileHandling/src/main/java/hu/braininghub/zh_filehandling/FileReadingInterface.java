package hu.braininghub.zh_filehandling;

import java.util.List;

/**
 *
 * @author Zsolt
 */
public interface FileReadingInterface {

    public List<String> readFromFileToList(String file);
}
