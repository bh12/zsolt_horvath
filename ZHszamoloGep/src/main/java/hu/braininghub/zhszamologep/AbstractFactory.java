package hu.braininghub.zhszamologep;

/**
 *
 * @author Zsolt
 */
public abstract class AbstractFactory {

    abstract Calculator getCalculator(String calculatorType);
}
