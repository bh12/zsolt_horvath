//Keszitsen egy kezdetleges aknakeresot! A palyameretet lehessen paranccsori argumentbol megadni.
//NxM-es legyen a palyameret. A palyameret teruletenek minimum negyedere maximum felere generaljon aknakat.
//A jatekos kezdjen tippelni koordinatakra a palyan belul.
//A program jelezze ha aknara lepett a felhasznalo ellenkezop esetben irja ki, tulelted.
//Ilyenkor a jatek veget er es a program kiirja mennyi mezot sikerult lepni robbanas nelkul.
//Plusz feladat: Amikor a felhasznalo tippel egyet, jelezze a felhasznalonak, hogy mennyi akna van korulotte.
//Ha 1 akna van korulotte irja ki: Nincs para csak 1 akna van korulotted.
//Ha 2-4: Ovatosan kezd melegedni a helyzet. Ha 4-nel tobb akkor irja ki: Hurt Locker!!
//Plusz feladat: Legyen paranccsori argumentumbol megadhato, hogy mennyi elete van a felhasznalonak.
//Aknaralepesnel egy eleteropontot veszit a jatekos.
//Akkor er veget ebben az esetben a jatek ha a jatekos osszes eleteropontja elfogyott.
package pkg6.ora_feladatok;

//@author Zsolt Horváth
import java.util.Random;
import java.util.Scanner;

public class Task2_Aknakereso {

    public static void main(String[] args) {
       
        //pálya létrehozása
        boolean[][] stage = createStage(stageSizeInput());
        //bombák elhelyezése
        fillStage(stage, calculeNumberOfBombs(stage));
        //pálya kirajzolása tesztelés céljából
        printStage(stage);
        boolean isBombHere = false;
        int stepCounter = 0;
        do {
            //játékos tipp átadása
            int[] userTipXY = userTip();
            // Van-e bomba a tippelt helyen?
            if (stage[userTipXY[0]][userTipXY[1]]) {
                isBombHere = true;
            } else {
                stepCounter++;
                System.out.println("Túlélted");
            }
           
        } while (!isBombHere);
        System.out.println("Meghaltál");
        System.out.println("Sikeres lépéseid száma: " + stepCounter);
    }
    public static int[] stageSizeInput() {
        Scanner sc = new Scanner(System.in);
        int[] stageWidthLength = new int[2];
        System.out.println("Add meg a pálya méretét: ");
        System.out.println("Szélesség: ");
        stageWidthLength[0] = sc.nextInt();
        System.out.println("Hosszúság: ");
        stageWidthLength[1] = sc.nextInt();
        return stageWidthLength;
    }
    public static boolean[][] createStage(int[] stageSize) {
        int width = stageSize[0];
        int length = stageSize[1];
        boolean[][] stage = new boolean[width][length];
        return stage;
    }
    public static int calculeNumberOfBombs(boolean[][] stage) {
        int stageArea = stage.length * stage[0].length;
        int bombsMinimum = stageArea / 4;
        System.out.println("min: " + bombsMinimum);
        int bombsMaximum = stageArea / 2;
        System.out.println("max: " + bombsMaximum);
        int numberOfBombs = (int) (Math.random() * (bombsMaximum - bombsMinimum + 1)) + bombsMinimum;
        return numberOfBombs;
    }
    public static void fillStage(boolean[][] stage, int numberOfBombs) {
        int rows = stage.length;
        int columns = stage[0].length;
        for (int counter = 0; counter < numberOfBombs; counter++) {
            int randomRow;
            int randomColumn;
            do {
                randomRow = new Random().nextInt(rows);
                randomColumn = new Random().nextInt(columns);
            } while (stage[randomRow][randomColumn]);
            stage[randomRow][randomColumn] = true;
        }
    }
    public static void printStage(boolean[][] stage) {
        for (int outerCounter = 0; outerCounter < stage.length; outerCounter++) {
            System.out.println("");
            for (int innerCounter = 0; innerCounter < stage[outerCounter].length; innerCounter++) {
                if (stage[outerCounter][innerCounter] == true) {
                    System.out.print("  X  ");
                } else {
                    System.out.print("  O  ");
                }
            }
            System.out.println("");
        }
    }
    public static int[] userTip() {
        Scanner sc = new Scanner(System.in);
        int[] playedCoordinates = new int[2];
        System.out.println("Kérem a koordinátákat: ");
        System.out.println("X: ");
        playedCoordinates[0] = sc.nextInt();
        System.out.println("Y: ");
        playedCoordinates[1] = sc.nextInt();
        return playedCoordinates;
    }
}
