package hu.braininghub.service;

import hu.braininghub.dao.ProductDAO;
import hu.braininghub.dto.ProductDTO;
import hu.braininghub.entity.ProductEntity;
import java.util.List;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.inject.Inject;

/**
 *
 * @author User
 */
@Singleton
@TransactionAttribute
public class ProductService {

    @Inject
    ProductDAO productDAO;

    public List<ProductEntity> findAll() {
        return productDAO.getAllProducts();
    }
}
