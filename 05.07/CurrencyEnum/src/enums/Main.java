// Hozz létre egy Currency enumot. Tároljuk el benne a pénznemet (EUR, GBP, HUF, USD, JPN, RMB)
// és a forintbeli értékét integerként. Hozzunk létre egy Person osztályt is,
// amiben minden személynél eltároljuk, hogy melyik pénznemből hány darab van neki.
// Tároljuk el még a személy korát és nevét is.
// Generáljunk véletlenszerűen embereket különböző típusú és mennyiségű pénzzel.
//             Írjuk ki a konzolra a következőket:
//  a.  az emberek vagyonának összértékét
//  b.  az emberek vagyonának átlagát
//  c.  az emberek vagyonának átlagát 18-49 korosztályban
//  d.  az emberek vagyonának mediánját
//  e.  az emberek nevét vagyonát növekvő sorrendben
package enums;


//@author Zsolt Horváth
public class Main {

    

    public static void main(String[] args) {

    Person.bornAPerson();
        System.out.println(Person.sumOfCurrency());
        
        
    }
}
