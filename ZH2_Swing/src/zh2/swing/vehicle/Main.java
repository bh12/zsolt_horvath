package zh2.swing.vehicle;

import java.util.Scanner;
import zh2.swing.vehicle.controller.Controller;

//@author Zsolt Horváth
public class Main {

    public static Scanner sc = new Scanner(System.in);
    public static int option;

    public static void main(String[] args) {
        
        // A konzolos és ablakos verzió ugyan abba a listába teszi az új járművet.
        Controller cl = new Controller();
        cl.startApp();

    }
}
