/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.zhszamologep;

/**
 *
 * @author Zsolt
 */
public class Main {

    public static void main(String[] args) {
        AbstractFactory calcFactory = FactoryProducer.getFactory(true);
        Calculator oldCalc = calcFactory.getCalculator("Old");
        System.out.println(oldCalc.getSum(5));
        System.out.println(oldCalc.getMulti(2));
        
    }

}
