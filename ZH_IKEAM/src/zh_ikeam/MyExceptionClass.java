package zh_ikeam;
//@author Zsolt Horváth

public class MyExceptionClass extends Exception {

    public MyExceptionClass(String s) {

        super(s);
    }

    @Override
    public String toString() {
        return "MyExceptionClass{"+ "Tönkrement a terméked "+ '}';
    }
    
}
