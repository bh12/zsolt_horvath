package enums;

/**
 *
 * @author Zsolt
 */
public enum CurrencyEnum {

    EUR(352),
    GBP(402),
    HUF(1),
    USD(323),
    JPN(3),
    RMB(45);

    private final int value;

    private CurrencyEnum(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public int switchToHuf(CurrencyEnum ce, int amount) {
        return ce.getValue() * amount;
    }
}
