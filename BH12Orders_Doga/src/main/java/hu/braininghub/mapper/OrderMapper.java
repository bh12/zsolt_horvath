package hu.braininghub.mapper;

import hu.braininghub.dto.OrderDTO;
import hu.braininghub.entity.OrderEntity;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 *
 * @author User
 */
@Mapper
public interface OrderMapper {

    OrderMapper INSTANCE = Mappers.getMapper(OrderMapper.class);

    OrderDTO toDTO(OrderEntity orderEntity);

    List<OrderDTO> toDTOList(List<OrderEntity> orderEntity);
}
