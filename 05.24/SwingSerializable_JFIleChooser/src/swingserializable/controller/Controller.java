package swingserializable.controller;
//@author Zsolt Horváth


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import javax.swing.JFileChooser;
import javax.swing.JTextArea;
import swingserializable.InputStream;
import swingserializable.SerializableBook;
import swingserializable.view.View;

public class Controller {

    private View view;
    private JFileChooser fileChooser;

    public Controller() {

        this.view = new View(this);
        view.init();
    }

    public File openFile() {
        fileChooser = new JFileChooser();
        int a = fileChooser.showOpenDialog(null);

        File file;
        if (a == JFileChooser.APPROVE_OPTION) {
            return file = fileChooser.getSelectedFile();
        }
        return null;
    }

    public void loadDataFromFile() {
        JTextArea text = view.getText();

        try (BufferedReader in = new BufferedReader(new FileReader(openFile()));) {
            String line = in.readLine();
            while (line != null) {
                text.append(line + "\n");
                line = in.readLine();
            }
        } catch (IOException e) {
            e.getStackTrace();

        }
    }

    public void saveDataFromJTextArea() {

        try (FileWriter fw = new FileWriter("kimentett_JTextArea.txt");) {
            view.getText().write(fw);
            System.out.println("A TextArea kimentve!");

        } catch (IOException e) {
            System.out.println("Nincs mit kimenteni");
        }

    }

    public void serializableObj() {

        try (FileOutputStream fos = new FileOutputStream("book.ser");
                ObjectOutputStream oos = new ObjectOutputStream(fos)) {
            SerializableBook book = new SerializableBook("Bela", 200, 2008);
            oos.writeObject(book);
        } catch (IOException e) {
            e.getStackTrace();
        }

    }

    public void backSerializableObj() {

        try (FileInputStream fis = new FileInputStream("book.ser");
                InputStream<SerializableBook> ois = new InputStream<>(fis)) {
            String line;
            SerializableBook book = ois.readObjectGeneric();

            view.getText().append(book.toString());

        } catch (FileNotFoundException ex) {
            ex.getStackTrace();
        } catch (IOException ex) {
            ex.getStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.getStackTrace();
        }
    }

    public void handleSerializableButton() {
        serializableObj();
    }

    public void handleBackSerializableButton() {
        view.setScreenText("");
        backSerializableObj();
    }

    public void handleSaveButton() {
        saveDataFromJTextArea();
    }

}
