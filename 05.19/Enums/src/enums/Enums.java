// Készíts egy napok ENUM-ot, ahol minden nap lehet egy sayHello üzenetet kiíratni,
// ami minden napnál más, és az átlagos aktuális hangulatot képviseli.
package enums;


//@author Zsolt Horváth

public class Enums {

    
    public static void main(String[] args) {
        System.out.println(Days.FRIDAY.getSayHello());
        System.out.println(Days.SATURDAY.getSayHello());
        System.out.println(Days.MONDAY.getSayHello());
        System.out.println(Days.THURSDAY.getSayHello());
    }

}
