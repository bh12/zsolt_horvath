
package banksimulation.model;

//@author Zsolt Horváth

import banksimulation.BankApplication;
import static banksimulation.BankApplication.simulateTransfer;
import banksimulation.exceptions.BankingException;
import banksimulation.exceptions.IllegalTargetClientException;
import banksimulation.exceptions.InsufficientFundsException;
import java.util.List;



public class Bank {
    private String name;

    public Bank(String name) {
        this.name = name;
    }

    
    public String getName() {
        return name;
    }

    public void bankInit(){
        BankApplication.banks.add((Bank) BankApplication.clients);
        
    }
    public static void simulate(List<Transfer> transfers, List<BankingException> bankingExceptions) {
        for (Transfer t : transfers) {
            try {
                simulateTransfer(t);
            } catch (BankingException e) {
                bankingExceptions.add(e);
            }
        }
    }
    
    public static void simulateTransfer(Transfer t) throws InsufficientFundsException, IllegalTargetClientException {
        Client source = t.getSource();
        Client target = t.getTarget();

        long ammount = t.getAmmount();

        long sourceBalance = source.getBalance();
        long targetBalance = target.getBalance();
        if (sourceBalance >= ammount) {
            source.setBalance(sourceBalance - ammount);
            target.setBalance(targetBalance + ammount);
        } else {
            throw new InsufficientFundsException(t);
        }

        if (source.equals(target)) {
            throw new IllegalTargetClientException(t);
        }
    }
}
