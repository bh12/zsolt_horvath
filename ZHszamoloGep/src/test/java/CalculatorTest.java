
import hu.braininghub.zhszamologep.Calculator;
import hu.braininghub.zhszamologep.CalculatorOld;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Zsolt
 */
public class CalculatorTest {

    Calculator calc;

    public CalculatorTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        calc = new CalculatorOld();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void getSum30_equals30() {
        long result = (long) calc.getSum(30);

        assertEquals(30, result);
    }
}
