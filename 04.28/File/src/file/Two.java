// Írjuk ki a felhasználó által megadott útvonalban lévő összes fájl illetve mappa nevet.
package file;

//@author Zsolt Horváth
import java.io.File;
import java.util.Scanner;

public class Two {

    public static void main(String[] args) {
        Scanner sc = new Scanner (System.in);
        
        String path = sc.nextLine();
        
        File f = new File(path);
        
        for (String s : f.list()){
            System.out.println(s);
        }
        
        
    }
}
