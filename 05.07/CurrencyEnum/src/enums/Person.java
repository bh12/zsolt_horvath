package enums;

//@author Zsolt Horváth
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

public class Person {

    public static Random rnd = new Random();
    public static Scanner sc = new Scanner(System.in);

    private int age;
    private String name;

    public static Map<CurrencyEnum, Integer> money = new HashMap<>();
    public static List<Person> person = new ArrayList<>();

    public static Person bornAPerson() {
        Person person = new Person();
        setName();
        setAge();
        setMoney();
        return person;

    }

    public static int sumOfCurrency() {

        int sum = 0;

        for (Person p : person) {
            for (CurrencyEnum money : p.getMoney().keySet()) {
                sum += money.switchToHuf(money, p.getMoney().get(money));

            }
        }
        return sum;
    }

    public Map<CurrencyEnum, Integer> getMoney() {
        return money;
    }

    public static void setMoney() {
        uploadCurrency();
    }

    private static void addMoney(CurrencyEnum ce, int amount) {
        money.put(ce, amount);
    }

    public static void uploadCurrency() {

        addMoney(CurrencyEnum.EUR, rnd.nextInt(30));
        addMoney(CurrencyEnum.GBP, rnd.nextInt(30));
        addMoney(CurrencyEnum.HUF, rnd.nextInt(50_000));
        addMoney(CurrencyEnum.JPN, rnd.nextInt(30));
        addMoney(CurrencyEnum.RMB, rnd.nextInt(30));
        addMoney(CurrencyEnum.USD, rnd.nextInt(30));

    }

    public int getAge() {
        return age;
    }

    public static void setAge() {
        System.out.println("Add meg az emberünk korát:");
        int creatAnAge = sc.nextInt();

    }

    public String getName() {
        return name;
    }

    public static void setName() {
        System.out.println("Add meg az emberünk nevét:");
        String creatAName = sc.next();

    }

}
