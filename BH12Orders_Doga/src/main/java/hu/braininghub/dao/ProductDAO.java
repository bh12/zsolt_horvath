package hu.braininghub.dao;

import hu.braininghub.entity.ProductEntity;
import java.util.List;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author User
 */
@Singleton
@TransactionAttribute
public class ProductDAO {

    private static final String QUERY_FIND_BY_FILTER = "select p from ProductEntity p ";

    @PersistenceContext
    private EntityManager em;

    public List<ProductEntity> getAllProducts() {
        TypedQuery<ProductEntity> namedQuery = em.createNamedQuery(ProductEntity.QUERY_FIND_ALL, ProductEntity.class);
        return namedQuery.getResultList();
    }

}
