package banksimulation.dataload;

import banksimulation.BankApplication;
import banksimulation.model.Client;
import banksimulation.model.Bank;
import banksimulation.model.Transfer;
import java.util.Date;
import java.util.List;

public class JavaDataLoader implements DataLoader {

    @Override
    public void loadInitialDataClient() {
        for (int i = 0; i < 10; i++) {
            BankApplication.clients.add(generateRandomClient());
        }

        for (int i = 0; i < 10; i++) {
            BankApplication.transfers.add(generateTransfer(BankApplication.clients));
        }
    }

    @Override
    public void loadInitialDataBank() {
        for (int i = 0; i < 3; i++) {
            BankApplication.banks.add(generateRandomBankId());
        }

    }

    public static Transfer generateTransfer(List<Client> clients) {
        Transfer t = new Transfer();

        int randomSourceIndex = (int) (Math.random() * clients.size());
        int randomTargetIndex = (int) (Math.random() * clients.size());

        t.setSource(clients.get(randomSourceIndex));
        t.setTarget(clients.get(randomTargetIndex));

        long ammount = (long) (Math.random() * BankApplication.MAX_TRANSFER_AMMOUNT);

        t.setAmmount(ammount);

        t.setDateOfCompletion(new Date());

        return t;
    }

    public static Client generateRandomClient() {
        long balance = (long) (Math.random() * 100000 + BankApplication.CLIENT_BASE_BALANCE);
        String clientId = generateRandomClientId();
        String accountNumber = generateRandomAccountNumber();

        Client c = new Client(clientId, accountNumber, balance);

        return c;
    }

    public static String generateRandomString(int length, int min, int max) {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < length; i++) {
            char randomChar = (char) (Math.random() * (max - min + 1) + min);
            sb.append(randomChar);
        }

        return sb.toString();
    }

    public static String generateRandomClientId() {
        return generateRandomString(5, 65, 90);
    }

    public static Bank generateRandomBankId() {

        Bank b = new Bank(generateRandomString(3, 65, 90));
        return b;
    }

    public static String generateRandomAccountNumber() {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < 15; i++) {
            char randomChar = (char) (Math.random() * 46 + 48);
            sb.append(randomChar);
        }

        return generateRandomString(15, 48, 90);
    }

    @Override
    public void loadInitialDataTransfer() {
       
    }

}
