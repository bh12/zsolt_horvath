package banksimulation;

import banksimulation.dataload.DataLoader;
import banksimulation.dataload.FileDataLoader;
import banksimulation.dataload.JavaDataLoader;
import banksimulation.exceptions.IllegalTargetClientException;
import banksimulation.exceptions.InsufficientFundsException;
import banksimulation.exceptions.BankingException;
import banksimulation.model.Bank;
import banksimulation.model.Client;
import banksimulation.model.Transfer;
import java.util.ArrayList;
import java.util.List;

public class BankApplication {

    public static final int CLIENT_BASE_BALANCE = 200000;
    
    public static final int MAX_TRANSFER_AMMOUNT = 200000;
    
    public static List<Bank> banks = new ArrayList<>();
    public static List<Client> clients = new ArrayList<>();
    public static List<Transfer> transfers = new ArrayList<>();
    public static List<BankingException> bankingExceptions = new ArrayList<>();

    public static void main(String[] args) {
        DataLoader loader = new FileDataLoader();
        loader.loadInitialDataClient();
        loader.loadInitialDataBank();
        loader.loadInitialDataTransfer();
        
        printClientDetails(clients, "kezdő");

        printTransfers(transfers);
        Bank.simulate(transfers, bankingExceptions);
//        simulate(transfers, bankingExceptions);
        
        printClientDetails(clients, "végső");
        
        printErrors(bankingExceptions);
        
        for(Bank b : banks){
            System.out.println(b.getName());
        }
    }

    public static void printTransfers(List<Transfer> transfers) {
        System.out.println("Utalások adatai:");
        for (Transfer t : transfers) {
            System.out.print("Küldő:" + t.getSource().getClientId());
            System.out.print("\t | Kedvezményezett:" + t.getTarget().getClientId());
            System.out.print("\t | Összeg:" + t.getAmmount());
            System.out.print("\t | Teljesítés dátuma:" + t.getDateOfCompletion());
            System.out.println("");
        }
    }

    public static void printErrors(List<BankingException> bankingExceptions) {
        System.out.println("Hiba lista:");
        for (BankingException b : bankingExceptions) {
            System.out.println(b.getMessage() + " |");
            System.out.print(b.getTransfer().toString());
            System.out.println("");
        }
    }

    public static void simulate(List<Transfer> transfers, List<BankingException> bankingExceptions) {
        for (Transfer t : transfers) {
            try {
                simulateTransfer(t);
            } catch (BankingException e) {
                bankingExceptions.add(e);
            }
        }
    }
    
    public static void printClientDetails(List<Client> clients, String balanceStatus) {
        System.out.println("Kliensek azonosítói és " + balanceStatus + " egyenlegei:");
        for (Client c : clients) {
            System.out.print("Azonosító:" + c.getClientId());
            System.out.print("\t | " + balanceStatus + " egyenleg:" + c.getBalance());
            System.out.println("");
        }
    }

    public static void simulateTransfer(Transfer t) throws InsufficientFundsException, IllegalTargetClientException {
        Client source = t.getSource();
        Client target = t.getTarget();

        long ammount = t.getAmmount();

        long sourceBalance = source.getBalance();
        long targetBalance = target.getBalance();
        if (sourceBalance >= ammount) {
            source.setBalance(sourceBalance - ammount);
            target.setBalance(targetBalance + ammount);
        } else {
            throw new InsufficientFundsException(t);
        }

        if (source.equals(target)) {
            throw new IllegalTargetClientException(t);
        }
    }

    
}
