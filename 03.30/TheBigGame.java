/*
 feladat: 
 írjunk karakteres játékot ahol a felhasználó egy pályán képes fel-le-jobbra-balra mozogni.
 Célja a kapu elérése, anélkül hogy bombára lépne. Játék közben számoljuk a lépései számát.
 Első változatban random generálunk játékost, kaput és bombát, odafigyelve rá, hogy ezek külön pozicióba legyenek.
 Házi feladat: 
 egészítsük ki a feladatot úgy, hogy több bomba legyen (pl. 5)
 
 */
package thebiggame;

//@author Zsolt Horváth
import java.util.Scanner;

public class TheBigGame {

    public static final int FIELD_SIZE = 5;
    public static final int[][] field = new int[FIELD_SIZE][FIELD_SIZE];
    public static final char FIELD_WALL = '|';

    public static int FIELD_X;
    public static int FIELD_Y;

    public static int PLAYER_X;
    public static int PLAYER_Y;

    public static int BOMB_X;
    public static int BOMB_Y;

    public static int GATE_X;
    public static int GATE_Y;

    public static final int EMPTY = 0;
    public static final char EMPTY_CHAR = '*';
    public static final int PLAYER = 1;
    public static final char PLAYER_CHAR = 'P';
    public static final int BOMB = 2;
    public static final char BOMB_CHAR = 'B';
    public static final int GATE = 3;
    public static final char GATE_CHAR = 'E';

    public static final char UP = 'w';
    public static final char DOWN = 's';
    public static final char LEFT = 'a';
    public static final char RIGHT = 'd';
    public static final char EXIT = 'x';

    public static final Scanner sc = new Scanner(System.in);

    public static int stepCounter = 0;
    public static int bombCounter = 5;
    public static final int bombArray[][] = new int[bombCounter][2];

    public static void main(String[] args) {
        initEvrything();
        play();

        //      az eltárolt bombák ellenőrzése
//        for (int i = 0; i < bombArray.length; i++) {
//            for (int j = 0; j < bombArray[i].length; j++) {
//                System.out.print(bombArray[i][j]);
//            }
//            System.out.println("");
//        }
    }

    public static void play() {
        printField();

        System.out.println("Kérem add meg a menetirányt:w,a,s,d,x");
        char step;
        do {
            step = sc.next().charAt(0);
            switch (step) {
                case UP:
                    move(PLAYER_X, PLAYER_Y - 1);
                    break;
                case DOWN:
                    move(PLAYER_X, PLAYER_Y + 1);
                    break;
                case LEFT:
                    move(PLAYER_X - 1, PLAYER_Y);
                    break;
                case RIGHT:
                    move(PLAYER_X + 1, PLAYER_Y);
                    break;
            }
            printField();

        } while (step != EXIT && !win() && !loose());
    }

    public static boolean win() {
        if (GATE_X == PLAYER_X && GATE_Y == PLAYER_Y) {
            System.out.println("Győztél! A lépéseid száma: " + stepCounter);
            return true;
        }
        return false;
    }

    public static boolean loose() {
        for (int i = 0; i < bombArray.length; i++) {

            if (PLAYER_X == bombArray[i][0] && PLAYER_Y == bombArray[i][1]) {
                System.out.println("Ennyi volt!");
                return true;
            }

        }
        return false;
    }

    public static void move(int newPlayerX, int newPlayerY) {
        if (!validStep(newPlayerX, newPlayerY)) {
            System.out.println("Fal!");
            return;
        }
        setPlayer(newPlayerX, newPlayerY);
        stepCounter++;
    }

    public static void setPlayer(int newPlayerX, int newPlayerY) {
        clearNewPlayerPos();
        PLAYER_X = newPlayerX;
        PLAYER_Y = newPlayerY;

        field[PLAYER_Y][PLAYER_X] = PLAYER;
    }

    public static void clearNewPlayerPos() {
        field[PLAYER_Y][PLAYER_X] = EMPTY;
    }

    public static boolean validStep(int newPlayerX, int newPlayerY) {
        if (newPlayerX < 0 || newPlayerX >= FIELD_SIZE || newPlayerY < 0 || newPlayerY >= FIELD_SIZE) {
            return false;
        }
        return true;
    }

    public static void initEvrything() {
        int[] temp = generateCoordinate();
        do {
            temp = generateCoordinate();
        } while (field[temp[1]][temp[0]] != EMPTY);

        PLAYER_X = temp[0];
        PLAYER_Y = temp[1];
        field[PLAYER_Y][PLAYER_X] = PLAYER;

        // annyi bomba generálódik, amekkora a bombArray.
        // itt tárolom el a bombákat egy segéd tömbbe. bombArray.
        // a bombArray-ben lévő értékek a bomba indexei.
        for (int i = 0; i < bombArray.length; i++) {
            do {
                temp = generateCoordinate();
            } while (field[temp[1]][temp[0]] != EMPTY);
            BOMB_X = temp[0];
            BOMB_Y = temp[1];
            bombArray[i][0] = BOMB_X;
            bombArray[i][1] = BOMB_Y;
            field[BOMB_Y][BOMB_X] = BOMB;

        }
        do {
            temp = generateCoordinate();
        } while (field[temp[1]][temp[0]] != EMPTY);

        GATE_X = temp[0];
        GATE_Y = temp[1];
        field[GATE_Y][GATE_X] = GATE;

    }

    public static int[] generateCoordinate() {

        int x = (int) (Math.random() * FIELD_SIZE);
        int y = (int) (Math.random() * FIELD_SIZE);

        if (field[y][x] != EMPTY) {
            return generateCoordinate();
        }

        int[] ret = new int[2];
        ret[0] = x;
        ret[1] = y;

        return ret;
    }

    public static void printField() {
        printWall();
        for (int i = 0; i < field.length; i++) {
            System.out.print(FIELD_WALL);
            for (int j = 0; j < field[i].length; j++) {
                switch (field[i][j]) {
                    case EMPTY:
                        System.out.print(EMPTY_CHAR);
                        break;
                    case PLAYER:
                        System.out.print(PLAYER_CHAR);
                        break;
                    case BOMB:
                        System.out.print(BOMB_CHAR);
                        break;
                    case GATE:
                        System.out.print(GATE_CHAR);
                        break;
                }
            }
            System.out.println(FIELD_WALL);
        }
        printWall();
    }

    public static void printWall() {
        for (int i = 0; i < field[0].length + 2; i++) {
            System.out.print(FIELD_WALL);
        }
        System.out.println("");

    }

}
