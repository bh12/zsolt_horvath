package car.servlets;
//@author Zsolt Horváth

import daos.BaseDao;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "CarServlet", urlPatterns = {"/CarServlet"})
public class CarServlet extends HttpServlet {
    BaseDao baseDao = new BaseDao();
    private static final String QUERY_INSERT_CAR = "INSERT INTO car (plate_number,color,engine_type,weigth,year_of_built,site_id)"
            + " VALUES (?,?,?,?,?,?);";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        req.getRequestDispatcher("Car.jsp").forward(req, res);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try (Connection conn = DriverManager.getConnection(baseDao.getCONNECTION_URL(), "admin", "admin");
                PreparedStatement ps = conn.prepareStatement(QUERY_INSERT_CAR)) {

            String plateNumber = req.getParameter("plate_number");
            String color = req.getParameter("color");
            String engineType = req.getParameter("engine_type");
            String weigth = req.getParameter("weigth");
            String yearOfBuilt = req.getParameter("year_of_built");
            String siteID = req.getParameter("site_id");

            ps.setString(1, plateNumber);
            ps.setString(2, color);
            ps.setString(3, engineType);
            ps.setString(4, weigth);
            ps.setString(5, yearOfBuilt);
            ps.setString(6, siteID);

            System.out.println("Hany sor insertalodott?" + ps.executeUpdate());
            
            resp.sendRedirect("MainServlet");  

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

}
