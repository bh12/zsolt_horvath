// Írj MVC-s SWING-es alkalmazást amin van egy darab textarea és négy darab gomb.
// Az első gombra kattintva egy Worker a másodikra egy Boss objektum jöjjön létre,
// úgy, hogy a textarea-ba logoljuk az eseményt (dátum-idő, milyen objektum jött létre). 
// Mentsük le a Boss-t és a Workereket egy listába és tudjuk őket serializálni és beolvasni (3. és 4. gomb).
// Mentés és beolvasás mehet fix fájlba.

package zh2_swing_gyakorlo;

import zh2_swing_gyakorlo.controller.Controller;


//@author Zsolt Horváth

public class Main {

    
    public static void main(String[] args) {
       Controller cl = new Controller();
    }

}
