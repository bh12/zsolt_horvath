package car.servlets;
//@author Zsolt Horváth

import daos.BaseDao;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "SiteServlet", urlPatterns = {"/SiteServlet"})
public class SiteServlet extends HttpServlet {
    
    BaseDao baseDao = new BaseDao();
    private static final String QUERY_INSERT_SITE = "INSERT INTO site (adress) VALUES (?);";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        req.getRequestDispatcher("Site.jsp").forward(req, res);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try (Connection conn = DriverManager.getConnection(baseDao.getCONNECTION_URL(), "admin", "admin");
                PreparedStatement ps = conn.prepareStatement(QUERY_INSERT_SITE)) {

            String adress = req.getParameter("adress");

            ps.setString(1, adress);

            System.out.println("Hany sor insertalodott?" + ps.executeUpdate());
            
            resp.sendRedirect("MainServlet");  

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
