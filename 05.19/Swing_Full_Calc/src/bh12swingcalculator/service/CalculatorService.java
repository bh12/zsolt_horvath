package bh12swingcalculator.service;

public interface CalculatorService {

    public int addNumbers(int n1, int n2);

    public int multipleNumbers(int n1, int n2);

}
