package zh2.swing.vehicle.view;
//@author Zsolt Horváth

import java.awt.Font;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import zh2.swing.vehicle.controller.Controller;

public class View extends JFrame {

    Controller controller;

    private JPanel panel;
    private JTextArea text;
    private JButton button1;
    private JButton button2;
    private JScrollPane scroll;

    public View(Controller controller) {
        this.controller = controller;
    }

    public void init() {
        setUpWindow();
        setUpCreatBmwButton();
        setUpCreatAirplaneButton();
        showWindow();
    }

    private void setUpWindow() {
        this.setSize(500, 500);
        this.setLocationRelativeTo(null);
        this.setTitle("VehicleMaker");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(new GridLayout(2, 1));
        setUpTextArea();
        this.panel = new JPanel();
        add(this.panel);

    }

    private void showWindow() {
        this.setVisible(true);
    }

    private void setUpTextArea() {
        this.text = new JTextArea(10, 20);
        this.text.setLineWrap(true);
        Font f = new Font("Arial", Font.BOLD, 20);
        this.text.setFont(f);
        this.scroll = new JScrollPane(this.text);
        add(this.scroll);
    }

    private void setUpCreatBmwButton() {
        this.button1 = new JButton("new BMW");
        this.button1.addActionListener(event -> {
            controller.handleCreatBmwButton();
        });

        panel.add(this.button1);
    }

    private void setUpCreatAirplaneButton() {
        this.button2 = new JButton("new Airplane");
        this.button2.addActionListener(event -> {
            controller.handleCreatAirplaneButton();
        });

        panel.add(this.button2);
    }

    
    public JTextArea getText() {
        return text;
    }

    public void setScreenText(String text) {
        this.text.setText(text);

    }

}
