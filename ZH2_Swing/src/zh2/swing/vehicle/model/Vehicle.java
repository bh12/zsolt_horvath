
package zh2.swing.vehicle.model;

/**
 *
 * @author Zsolt
 */
public abstract class Vehicle {

    private int serialNumber;

    public int getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(int sirialNumber) {
        this.serialNumber = sirialNumber;
    }
    
    public int makeASerialNumber(){
        int random = (int)(Math.random()*100)+1;
        return this.serialNumber = random;
    }

}
