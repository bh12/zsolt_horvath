package hu.braininghub.zh_filehandling;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Zsolt
 */
public class ReadFromFile implements FileReadingInterface {

    @Override
    public List<String> readFromFileToList(String file) {
        List<String> line = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {

            String s;
            while ((s = br.readLine()) != null) {
                line.add(s);
                System.out.println(s);
            }
        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {
        }
        return line;
    }

}
