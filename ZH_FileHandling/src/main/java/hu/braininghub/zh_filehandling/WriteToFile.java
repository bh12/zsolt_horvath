package hu.braininghub.zh_filehandling;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 *
 * @author Zsolt
 */
public class WriteToFile implements FileWriterInterface {

    @Override
    public void writeToFileEvrySecondLines(String fileName, List<String> lines) {
        List<String> filteredList = IntStream.range(0, lines.size()).filter(n -> n % 2 == 1).mapToObj(lines::get).collect(Collectors.toList());
        try (FileWriter writer = new FileWriter(fileName)) {
            filteredList.forEach((String str) -> {
                try {
                    writer.write(str + System.lineSeparator());
                } catch (IOException ex) {
                }
            });
        } catch (IOException ex) {
        }
    }

}
