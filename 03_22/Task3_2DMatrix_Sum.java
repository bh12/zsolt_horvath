//3.  A program állítson elő véletlenszerűen két megegyező méretű mátrixot!
//    Számítsa ki és írja ki a két mátrix összegét!
//    Minden részfeladatot külön metódus valósítson meg!
package hometask5;

//@author Zsolt Horváth
public class Task3_2DMatrix_Sum {

    public static void main(String[] args) {
        
       int arrayX = randomItem();
       int arrayY = randomItem();

        int[][] array1 = new int[arrayX][arrayY];
        int[][] array2 = new int[arrayX][arrayY];

        printArray1(fillArray1(array1));
        System.out.println("");
        printArray1(fillArray1(array2));
        sumArray(array1, array2);
        System.out.println("A tömbök értékeinek összege: " + sumArray(array1, array2));
    }

    public static void printArray1(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println("");
        }
    }

    public static int[][] fillArray1(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = (int) (Math.random() * 90 + 10);
            }
        }
        return array;
    }
    
    
    public static int randomItem() {
        int randomNumber = (int) (Math.random() * 5 + 2);

        return randomNumber;

    }

    public static int sumArray(int[][] array, int[][] array2) {
        int sum = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                sum += array[i][j];
            }
        }
        for (int i = 0; i < array2.length; i++) {
            for (int j = 0; j < array2[i].length; j++) {
                sum += array2[i][j];
            }
        }

        return sum;

    }

}
