
package fruits;

//@author Zsolt Horváth


public class Banana extends Fruit {

    public Banana() {
    }

    @Override
    public void setPrice(double price) {
        this.price = price/100;
    }

    @Override
    public String toString() {
        return "Banana{ A színe: " +super.getColor()+" és a dkg ára: "+super.getPrice()+" Ft."+ '}';
    }
    
    

}
