// Készítsen lottó szelvény készítő programot. A program a felhasználó által megadott darabszámú
// lottó szelvényt generáljon, és mindet egy külön fájlba írja ki olyan formátumban,
// hogy majd ezt később be is tudjuk olvasni.
package Six.and.Seven.lotto;

//@author Zsolt Horváth
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import javafx.scene.input.DataFormat;

public class Six_and_Seven_lotto {

    public static final Scanner sc = new Scanner(System.in);
    public static List<String> namesOfGames = new ArrayList<>();

    public static void main(String[] args) {
        System.out.println("Kérem, adja meg,hány szelvényt szeretne kitölteni: ");
        int numberOfgames = sc.nextInt();
        int counterOfGames = 1;

        while (numberOfgames != 0) {
            int[] lotto = new int[5];

            for (int i = 0; i < lotto.length; i++) {
                int randomNumber = (int) (Math.random() * 90) + 1;

                boolean usedNumber = false;

                for (int j = 0; j < lotto.length; j++) {
                    if (randomNumber == lotto[j]) {
                        usedNumber = true;
                    }
                }
                if (!usedNumber) {
                    lotto[i] = randomNumber;
                } else {
                    i--;
                }
            }

            Date now = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("hh mm ss");
            String time = dateFormat.format(now);
            File dir = new File(time);
            dir.mkdir();

            File f = new File(dir, "lotto" + counterOfGames + ".txt");
            // listában tárolom az elérési útvaonalakat, így bárhonnan ki lehet printelni.
            namesOfGames.add(f.getAbsolutePath());

            int counterOfNumbers = 1;
            try (BufferedWriter outputWriter = new BufferedWriter(new FileWriter(f))) {
                for (int i = 0; i < lotto.length; i++) {

                    outputWriter.write(counterOfNumbers + ". " + Integer.toString(lotto[i]) + "\n");
//                    outputWriter.newLine();  // ez is egy lehetőség az új sorhoz.
                    counterOfNumbers++;

                }
                System.out.println("A " + counterOfGames + ". szelvény kitöltve!");
            } catch (IOException ex) {
                System.out.println("Hiba");
            }

            counterOfGames++;
            numberOfgames--;
        }
//        Ellenőrzés
//        for (String s : namesOfGames) {
//            System.out.println(s);
//        }
//        System.out.println(namesOfGames.get(1));
//
        while (counterOfGames != 1) {
            printFile(namesOfGames.get(numberOfgames));
            numberOfgames++;
            counterOfGames--;
        }
    }

    public static void printFile(String s) {
        File f = new File(s);
        System.out.println(f.getAbsolutePath());
        try (BufferedReader br = new BufferedReader(new FileReader(f))) {
            String line;
            while ((line = br.readLine()) != null) {
                System.out.println(line);
            }
            System.out.println("\n");
        } catch (IOException e) {
            System.out.println("A file nem található!");
        }

    }

}
