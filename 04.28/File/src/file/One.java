// Írjuk ki egy fájl tulajdonságait (fájl-e?, útvonal, abszolút útvonal, szülőmappa,
// fájl neve, mérete, olvasható-e, írható-e, rejtett-e, utolsó módosítás dátuma)
package file;

import java.io.File;


//@author Zsolt Horváth

public class One {

    
    public static void main(String[] args) {
       File f = new File("e:/Zsolt/BRAINING_HUB/Junior/II.Modul/Orai/06.ora/test.txt");
        System.out.println(f.getAbsoluteFile());
        System.out.println(f.getAbsolutePath());
        System.out.println(f.getName());
        System.out.println(f.getParent());
        System.out.println(f.getPath());
        System.out.println(f.getTotalSpace());
        System.out.println(f.canRead());
        System.out.println(f.canWrite());
        System.out.println(f.lastModified());
        System.out.println(f.setWritable(true));
        
              
    }

}
