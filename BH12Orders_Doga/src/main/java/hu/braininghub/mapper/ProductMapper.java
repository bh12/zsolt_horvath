package hu.braininghub.mapper;

import hu.braininghub.dto.ProductDTO;
import hu.braininghub.entity.ProductEntity;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 *
 * @author User
 */
@Mapper
public interface ProductMapper {

    ProductMapper INSTANCE = Mappers.getMapper(ProductMapper.class);

    ProductDTO toDTO(ProductEntity productEntity);

    List<ProductDTO> toDTOList(List<ProductEntity> productEntity);
}
