<%-- 
    Document   : Car
    Created on : Jun 16, 2020, 7:40:47 PM
    Author     : Zsolt
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Add car Page</title>
    </head>
    <body>
        <h1>Add Car!</h1>
        <form action = "CarServlet" method = "POST">
            Plate Number: <input type = "text" name = "plate_number">
            <br />
            Color: <input type = "text" name = "color" />
            <br />
            Engine Type: <input type = "text" name = "engine_type" />
            <br />
            Weigth: <input type = "text" name = "weigth" />
            <br />
            Year of Built Type: <input type = "text" name = "year_of_built" />
            <br />
            SiteId:  <input type = "text" name = "site_id" />
            <br />
            <input type = "submit" value = "Submit" />
        </form>
    </body>
</html>
