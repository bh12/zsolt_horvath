// Kérjünk be a felhasználótól szöveges sorokat addig, amíg egy üres sort nem ír be.
// A sorokat sorszámmal írjuk ki egy fájlba „scanner_text.txt” néven.
package file;

//@author Zsolt Horváth
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Three {

    public static final Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {

        File f = new File("scanner_text.txt");

        int lineCounter = 1;

        try (FileWriter fw = new FileWriter(f)) {
            String line;

            do {
                line = sc.nextLine();
                fw.write(lineCounter + ". " + line + "\n");
                lineCounter++;

            } while (!line.equals(""));
        } catch (IOException e) {
            System.out.println("Hiba");
        }

    }
}
