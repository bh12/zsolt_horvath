package enums;

public enum Days {

    MONDAY("SHIT"),
    TUESDAY("AAHH"),
    WEDNESDAY("GRRRR"),
    THURSDAY("JUST ONE DAY"),
    FRIDAY("ITS OVER"),
    SATURDAY("SMILE"),
    SUNDAY("THIS IS THE END");

    public final String sayHello;

    private Days(String sayHello) {
        this.sayHello = sayHello;
    }
    
    public String getSayHello(){
        return this.sayHello;
    }

}
