package swingserializable.controller;
//@author Zsolt Horváth

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import swingserializable.InputStream;

import swingserializable.SerializableBook;
import swingserializable.model.Model;
import swingserializable.view.View;

public class Controller {

    private Model model;
    private View view;

    public Controller() {

        this.model = new Model();
        this.view = new View(this);
        view.init();
    }

    public void loadDataToTextArea() {

        try (FileReader reader = new FileReader(model.getFile());) {
            view.getText().read(reader, model.getFile());
            view.getText().append("\n Most txt fileból olvasunk!");

        } catch (IOException e) {
            System.out.println("A File nem található");
        }

    }

    public void loadDataFromFile() {

        try (FileReader reader = new FileReader(model.getFile());) {
            view.getText().read(reader, model.getFile());
            view.getText().append("\n Most txt fileból olvasunk!");

        } catch (IOException e) {
            System.out.println("A File nem található");
        }

    }

    public void saveDataFromJTextArea() {

        try (FileWriter fw = new FileWriter("kimentett_JTextArea.txt");) {
            view.getText().write(fw);
            System.out.println("A TextArea kimentve!");

        } catch (IOException e) {
            System.out.println("Nincs mit kimenteni");
        }

    }

    public void serializableObj() {

        try (FileOutputStream fos = new FileOutputStream("book.ser");
                ObjectOutputStream oos = new ObjectOutputStream(fos)) {
            SerializableBook book = new SerializableBook("Bela", 200, 2008);
            oos.writeObject(book);
        } catch (IOException e) {
            e.getStackTrace();
        }

    }

    public void backSerializableObj(){

        try (FileInputStream fis = new FileInputStream("book.ser");
                InputStream<SerializableBook> ois = new InputStream<>(fis)) {

            SerializableBook book = ois.readObjectGeneric();
            
            view.getText().append(book.toString());

        } catch (FileNotFoundException ex) {
            ex.getStackTrace();
        } catch (IOException ex) {
            ex.getStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.getStackTrace();
        }
    }

    public void handleSerializableButton() {
        serializableObj();
    }
    public void handleBackSerializableButton() {
        backSerializableObj();
    }

    public void handleSaveButton() {
        saveDataFromJTextArea();
    }

    public void handleReadFileButton() {
        loadDataToTextArea();

    }
}
