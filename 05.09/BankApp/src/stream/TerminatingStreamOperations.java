/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stream;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 *
 * @author kopacsi
 */
public class TerminatingStreamOperations {

    public static void main(String[] args) {
        Person p1 = new Person("Jani", 23);
        Person p2 = new Person("Fanni", 54);
        Person p3 = new Person("Fanni", 123);
        Person p4 = new Person("Bandi", 63);

        List<Person> persons = new ArrayList<>();
        persons.add(p1);
        persons.add(p2);
        persons.add(p3);
        persons.add(p4);

        Optional<Integer> fanni = findPersonStream(persons, "Fanni");
        if (fanni.isPresent()) {
            System.out.println(fanni.get());
        } else {
            System.out.println("Hová tűnt Fanni?");
        }

        List<Integer> ageOfPeople = persons.stream()
                .filter(x -> x.getName().equals("Fanni"))
                .filter(x -> x.getAge() > 10)
                .map(x -> x.getAge())
                .collect(Collectors.toList());

        System.out.println(ageOfPeople.size());

        System.out.println(findOldestPerson(persons).get().getName());
        
        System.out.println(allMatch63(persons));
        
        System.out.println(anyMatch63(persons));
    }

    public static Person findPerson(List<Person> persons, String name) {
        for (Person p : persons) {
            if (p.getName().equals(name)) {
                return p;
            }
        }
        return null;
    }

    public static Optional<Integer> findPersonStream(List<Person> persons, String name) {
        return persons.stream()
                .filter(x -> x.getName().equals(name))
                .filter(x -> x.getAge() > 10)
                .map(x -> x.getAge())
                .findFirst();
    }

    public static Optional<Person> findOldestPerson(List<Person> persons) {
        return persons.stream().max((o1, o2) -> {
            return o1.getAge() - o2.getAge();
        });
    }

    public static long count(List<Person> persons) {
        return persons.stream().filter(x -> x.getName().equals("Fanni"))
                .filter(x -> x.getAge() > 10).count();
    }
    
    public static boolean allMatch63(List<Person> persons) {
        return persons.stream().allMatch( x-> x.getAge() == 63);
    }
    
    public static boolean anyMatch63(List<Person> persons) {
        return persons.stream().anyMatch( x-> x.getAge() == 63);
    }
}
