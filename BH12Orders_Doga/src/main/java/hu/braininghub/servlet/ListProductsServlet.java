package hu.braininghub.servlet;

import hu.braininghub.dto.ListProducts;
import hu.braininghub.entity.ProductEntity;
import hu.braininghub.mapper.ProductMapper;
import hu.braininghub.service.ProductService;
import java.io.IOException;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author User
 */
@WebServlet(urlPatterns = "/listProducts")
public class ListProductsServlet extends HttpServlet {

    private static final String LIST_PRODUCTS = "listProducts";
    @Inject
    private ProductService productService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
<<<<<<< HEAD
        try {
            List<ProductEntity> products = productService.findAll();

            ListProducts listProducts = (ListProducts) req.getSession()
                    .getAttribute(LIST_PRODUCTS);

            if (listProducts == null) {
                listProducts = new ListProducts();
                req.getSession().setAttribute(LIST_PRODUCTS, listProducts);
            }
            System.out.println("izééé: " +products);
            listProducts.setProducts(ProductMapper.INSTANCE.toDTOList(products));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
=======

>>>>>>> a69b002af21da394b610919a5a7621fca0329841
        req.getRequestDispatcher("WEB-INF/listProducts.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

<<<<<<< HEAD
=======
        List<ProductEntity> products = productService.findAll();
        ListProducts listProducts = (ListProducts) req.getSession()
                .getAttribute(LIST_PRODUCTS);

        if (listProducts == null) {
            listProducts = new ListProducts();
            req.getSession().setAttribute(LIST_PRODUCTS, listProducts);
        }

        listProducts.setProducts(ProductMapper.INSTANCE.toDTOList(products));

>>>>>>> a69b002af21da394b610919a5a7621fca0329841
        resp.sendRedirect(req.getContextPath() + "/listBooks");
    }

}
