package hu.braininghub.dto;

import java.time.LocalDate;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author User
 */
@Getter
@Setter
public class OrderDTO extends BaseDTO {

    private Long id;
    private LocalDate orderDate;
    private LocalDate shippingDate;

}
