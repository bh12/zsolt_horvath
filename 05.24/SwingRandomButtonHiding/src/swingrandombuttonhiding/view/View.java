package swingrandombuttonhiding.view;
//@author Zsolt Horváth

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import swingrandombuttonhiding.controller.Controller;

public class View extends JFrame {

    private Controller controller;

    private JPanel panel;
    private JTextField screen;
    private JButton plusButton;
    private JButton multiButton;
    private JButton equalButton;
    private JButton clearButton;
    private JButton zeroButton;
    private JButton[] numberButtons = new JButton[9];

    public View(Controller controller) {
        this.controller = controller;
    }

    public void init() {
        setUpWindow();
        setUpNumberButtons();
        showWindow();
    }

    private void setUpWindow() {
        this.setSize(500, 500);
        this.setLocationRelativeTo(null);
        this.setTitle("MiniGame");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        panel = new JPanel();
        panel.setLayout(new GridBagLayout());

        add(panel);
    }

    private void showWindow() {
        this.setVisible(true);
    }

    private void setUpNumberButtons() {
        GridBagConstraints gbc = new GridBagConstraints();
        ActionListener numberListener = event -> {
            JButton jb = (JButton) event.getSource();
            controller.handleNumberButtonClick(jb.getText());
        };
        int num = 1;

        for (int i = 3; i >= 1; i--) {
            for (int j = 1; j < 4; j++) {
                JButton button = new JButton(Integer.toString(num));
                button.addActionListener(numberListener);
                gbc.gridx = j;
                gbc.gridy = i;
                gbc.weightx = 1;
                gbc.weighty = 1;
                gbc.gridwidth = 1;
                gbc.fill = GridBagConstraints.BOTH;
                panel.add(button, gbc);
                this.numberButtons[num - 1] = button;
                num++;
            }
        }
    }

    public void setScreenText(String screenText) {
        this.screen.setText(screenText);
    }

    public JButton[] getNumberButtons() {
        return numberButtons;
    }

    public void setNumberButtons(JButton[] numberButtons) {
        this.numberButtons = numberButtons;
    }

}
