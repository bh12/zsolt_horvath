<%-- 
    Document   : MainMenu
    Created on : Jun 16, 2020, 7:41:23 PM
    Author     : Zsolt
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Main page</title>
    </head>
    <body>
        <h1>Menu</h1>
        <a href="CarServlet" >Car Servlet</a><br/>
        <a href="SiteServlet" >Site Servlet</a><br/>
        <a href="BookingServlet" >Booking Servlet</a><br/>
    </body>
</html>
