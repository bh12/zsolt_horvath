//3.    Hozzunk létre egy 2D tömböt a következőnek megfelelően:
//az első dimenzióban tároljuk el az alkalmazottak azonosítóját (egész számok, 1-től növekedve 1-gyel).
//A második dimenzióban tároljuk el az adott alkalmazotthoz tartozó havi fizetésüket.
//A havi fizetés a következőképp alakul:
//a.    Értékesítő kollégákról van szó, a fizetésük függ attól, hogy mennyi árut adnak el.
//Az alap bérük 450.000 Ft, ehhez adjunk hozzá véletlenszerűen 50.000-125.000 Forintot.
//b.    Minden negyedév végén kapnak véletlenszerűen az adott havi fizetésükhöz képest 5-15 százalékot.
//Írasd  ki a kollégák fizetését hónapra bontva.
//4.    Az előző feladatot egészítsük ki a következő képpen.
// Adott a következő két String halmaz:
//a.    Laci, Józsi, Béla, Feri, Adri, Zsófi, Károly
//b.    Nagy, Veréb, Molnár, Kiss, Papp, Tóth
//Rendeljünk az előző feladatbeli azonosító számokhoz véletlenszerű neveket
//a fentebb felsorolt keresztnevek és vezetékneveket felhasználva, és így Írasd  ki a kollégák fizetéseit.

//+A két String halmazt kapja meg a program parancssorból. Ha nincs megadva adat parancssorból, akkor használjuk az eredeti halmazainkat.


package hometask7;

//@author Zsolt Horváth
import java.util.Scanner;

public class Task1_PayDay {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String secondName = "Nagy   ,Veréb ,Kiss  ,Kovács,Tóth  ,Papp  ,Molnár,Misuga,Herteld,Pakucs";
        String firstName = "Ond    ,Kond   ,Tas    ,Huba   ,Töhötöm,Morgó  ,Hapci  ,Tudor  ,Vidor  ,Szundi ";

        String[] familyName = secondName.split(",");
        String[] givenName = firstName.split(",");

        System.out.println("Add meg a dolgozók számát: ");
        int employeeNumber = sc.nextInt();

        int[][] employeeData = new int[employeeNumber + 1][13];

        employeePay(employeeData); // az alap fizetések feltöltése
        System.out.println("\n");
        System.out.println("Az alap fizetések:");
        printMounths();
        printPay(employeeData);

        System.out.println("");

        System.out.println("A fizetések havi bónusszal: ");
        printMounths();
        employeePayWithBonus(employeeData);
        printPay(employeeData);

        System.out.println("");
//        System.out.println(calculatePlus());  // a plus ellenőrzése 
        System.out.println("A negyed éves bónusszal ellátott fizetések:");
        employeePayWithPlus(employeeData);
        printPay(employeeData);

        System.out.println("");
        System.out.println("Az éves fizetések egyben:");
        sumPay(employeeData, employeeNumber);
        printPay(employeeData);

    }

    public static int calculateBonus() {
        return (int) (Math.random() * 75000 + 50000);
    }

    public static double calculatePlus() {
        return ((Math.random() * 10 + 5) / 100) + 1;
    }

    public static void employeePayWithPlus(int[][] employeePay) {
        
        for ( int i = 0 ; i <employeePay.length; i++){
        for ( int j = 0; j < employeePay.length; j+=3){
            employeePay[i][j] *= calculatePlus();
        }
        }
        
        
//        for (int i = 1; i < employeePay.length; i++) {
//            employeePay[i][2] *= calculatePlus();
//        }
//        for (int i = 1; i < employeePay.length; i++) {
//            employeePay[i][5] *= calculatePlus();
//        }
//        for (int i = 1; i < employeePay.length; i++) {
//            employeePay[i][8] *= calculatePlus();
//        }
//        for (int i = 1; i < employeePay.length; i++) {
//            employeePay[i][11] *= calculatePlus();
//        }
       
    }

    public static void employeePay(int[][] employeePayWithBonus) {
        for (int i = 0; i < employeePayWithBonus.length; i++) {
            for (int j = 0; j < employeePayWithBonus[i].length - 1; j++) {

                employeePayWithBonus[i][j] = 450_000;
            }
        }

    }

    public static void employeePayWithBonus(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length - 1; j++) {
                array[i][j] = 450_000 + calculateBonus();
            }
        }

    }

    public static void sumPay(int[][] employeePay, int employeeNumber) {

        int counter = 0;
        do {
            int sumPay = 0;
            for (int j = 0; j < employeePay[counter].length - 1; j++) {
                sumPay += employeePay[counter][j];
            }
            employeePay[counter][12] = sumPay;
            counter++;
        } while (counter <= employeeNumber);
    }

    public static void printPay(int[][] array) {
        for (int i = 1; i < array.length; i++) {
            System.out.print(i + "|" + "  ");
            for (int j = 0; j < array[i].length; j++) {
                System.out.printf(array[i][j] + "   ");
            }
            System.out.println("\n");
        }
    }

    public static void printMounths() {
        String[] months = {"Nr."+"\t","Jan"+"\t ","Feb"+"\t","Marc"+"\t  ","Apr"+"\t   ","Maj"+"\t    ","Jun"+"\t     ","Jul"+"      ","Aug"+"     ","Szep"+"\t","Okt"+"\t ","Nov"+"\t   ","Dec"+"\t ","Éves fizu"};

        for (int i = 0; i < months.length; i++) {
            System.out.printf(months[i]);
        }
        System.out.println("\n");

    }

}
