// készíts egy 5ös lottó sorsolást. a felhasználó adjon meg 5 számot
// és ha van találat, irassa ki.
package pkg6.ora_feladatok;

//@author Zsolt Horváth
import java.util.Scanner;

public class Task1_Lottozó {

    public static void main(String[] args) {

        lottoMatches(manualLotto(5), generatedLotto(5, 1, 90));
    }

    public static int[] manualLotto(int size) {
        int[] array = new int[size];
        Scanner sc = new Scanner(System.in);
        int lottoNumbers = 0;
        System.out.println("Kérlek add meg a számaid: ");
        for (int i = 0; i < array.length; i++) {

            lottoNumbers = sc.nextInt();

            boolean usedNumber = false;

            for (int j = 0; j < array.length; j++) {

                if (lottoNumbers == array[j]) {
                    System.out.println("Ezt a számot már megadtad.Másikat kérek.");
                    usedNumber = true;
                }
            }
            if (!usedNumber) {
                array[i] = lottoNumbers;
            } else {
                i--;
            }
        }
        System.out.println("Az általad megadott számok: ");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + ",");
        }
        System.out.println("");
        return array;
    }

    public static int[] generatedLotto(int size, int min, int max) {
        int[] array = new int[size];

        for (int i = 0; i < array.length; i++) {
            int randomNumber = (int) (Math.random() * (max - min + 1) + min);

            boolean usedNumber = false;

            for (int j = 0; j < array.length; j++) {
                if (randomNumber == array[j]) {
                    usedNumber = true;
                }
            }
            if (!usedNumber) {
                array[i] = randomNumber;
            } else {
                i--;
            }
        }
        System.out.println("A gép álltal sorsolt számok: ");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + ",");
        }
        System.out.println("");

        return array;

    }

    public static void lottoMatches(int[] array, int[] array2) {
        int counter = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array2.length; j++) {
                if (array[i] == array2[j]) {
                    counter++;
                }
            }
        }
       if (counter <= 1){
            System.out.println("Nincs találat.");
        } else{
           System.out.println("A találatok száma: " +counter);
       }

    }
}
