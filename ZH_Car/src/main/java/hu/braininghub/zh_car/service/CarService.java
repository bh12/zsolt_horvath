package hu.braininghub.zh_car.service;

import hu.braininghub.zh_car.dao.CarDAO;
import hu.braininghub.zh_car.dto.CarDTO;
import java.util.List;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.inject.Inject;

/**
 *
 * @author Zsolt
 */
@Singleton
@TransactionAttribute
public class CarService {

    @Inject
    private CarDAO carDAO;

    public List<CarDTO> getCars() {
        return carDAO.getCars();
    }

    public void addNewCar(String plateNumber, String brand, int mileage) {
        carDAO.addNewCar(plateNumber, brand, mileage);
    }

    public List<CarDTO> getLowestMileAgeCar() {
        return carDAO.getLowestMileAgeCar();
    }
}
