// A program kérjen be egy elérési és egy cél útvonalat.
// Az elérési útvonalon lévő fájlt tartalmával együtt másolja át a cél útvonalra.
// Ha a megadott fájl egy mappa, akkor dobjunk kivételt amit le is kezelünk. 
// Ha bármi hiba történik (például a cél útvonal ugyanaz mint az elérési útvonal)
// akkor is dobjunk kivételt, és kezeljük le.
package file;

//@author Zsolt Horváth
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Five {

    public static final Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
//        String sourcePath = sc.nextLine();
//        String targetPath = sc.nextLine();
//
//        File sorceFile = new File(sourcePath);
//        File targetFile = new File(targetPath);
//
//        try (BufferedReader br = new BufferedReader(new FileReader(sorceFile));
//                FileWriter fw = new FileWriter(targetFile)) {
//            String line = br.readLine();
//            while (line != null) {
//                System.out.println(line);
//                line = br.readLine();
//            }
//
//        } catch (IOException e) {
//
//        }
        int i = sc.nextInt();
         double d = sc.nextDouble();
         sc.nextLine();
        String s = sc.nextLine();
        
        
        System.out.println("String: " + s);
        System.out.println("Double: " +d);
        System.out.println("Int: " +i);
    }
}
