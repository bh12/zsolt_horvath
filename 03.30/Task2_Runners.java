//1. futóverseny eredményeinek feldolgozása: kérjük be a felhasználótól hány futó volt
// (legalább 3 ember kell a versenyhez), majd kérjük be a futók nevét és
// hogy hány perc alatt teljesítették a távot,
//majd a végén írjuk ki az első három helyezett futót és eredményeit.
package Hazifeladat_8;

//@author Zsolt Horváth
import java.util.Scanner;

public class Task2_Runners {

    public static final Scanner sc = new Scanner(System.in);
    public static int numberOfRunners;

    public static void main(String[] args) {

        numbers();

        sortArrayAsc(timesOfRunners(), namesOfRunners());

    }

    public static int[] timesOfRunners() {
        int[] array = new int[numberOfRunners];
        System.out.println("Kérlek, add meg a futók idejét percben: ");
        for (int i = 0; i < numberOfRunners; i++) {
            array[i] = sc.nextInt();
        }
        return array;
    }

    public static int numbers() {
        System.out.println("Add meg,hány futó volt a versenyen.");
        do {
            numberOfRunners = sc.nextInt();
        } while (numberOfRunners < 3);
        return numberOfRunners;
    }

    public static String[] namesOfRunners() {
        String[] namesOfRunners = new String[numberOfRunners];
        System.out.println("Kérlek, add meg a futók nevét: ");
        for (int i = 0; i < numberOfRunners; i++) {
            namesOfRunners[i] = sc.next();
        }
        return namesOfRunners;
    }

    public static void printItems(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println("");
    }

    public static void printItems(String title, int[] times, String[] names) {
        System.out.print(title);
        for (int i = 0; i < times.length; i++) {
            System.out.print(times[i] + " ");
        }
        System.out.println("");

        for (int i = 0; i < names.length; i++) {
            System.out.print(names[i] + " ");
        }
        System.out.println("");
    }

    public static void sortArrayAsc(int[] times, String[] names) {
        for (int i = 0; i < times.length; i++) {
            for (int j = times.length - 1; j > i; j--) {
                if (times[i] > times[j]) {//csere
                    int tmp = times[i];
                    String tmp1 = names[i];
                    times[i] = times[j];
                    names[i] = names[j];
                    times[j] = tmp;
                    names[j] = tmp1;

                }
            }
        }
        System.out.println("");
        System.out.println("Az eredmény: ");

        int k = 1;
        for (int i = 0; i < names.length; i++) {
            System.out.print(k + ":" + names[i] + ", ");
            k++;
        }
        System.out.println("");

        for (int i = 0; i < times.length; i++) {
            System.out.print(" " + times[i] + "   ");
        }
        System.out.println("");
    }
}
