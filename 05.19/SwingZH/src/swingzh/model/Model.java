package swingzh.model;
//@author Zsolt Horváth

import java.io.File;
import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.LinkedList;
import java.util.Queue;

public class Model {

    private Queue<String> list = new LinkedList<>();
    private int counter = 0;
    private File f = new File("date.txt");
    private SimpleDateFormat dateAndTime;
    private Date date;

    public String getLocalDate() {
        dateAndTime = new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z");
        date = new Date(System.currentTimeMillis());
        return dateAndTime.format(date).toString();

    }

    public File getF() {
        return f;
    }

    public int getCounter() {
        return counter++;
    }

    public void setCounter() {
        this.counter = counter;
    }


    public Queue<String> getList() {
        return list;
    }

    public void setList(String str) {
        this.list = list;
    }

}
