package hu.braininghub.dto;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author User
 */
@Getter
@Setter
public class ListOrders {

    List<OrderDTO> orders;
}
