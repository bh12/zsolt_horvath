//Fájlkezelés: írj alkalmazást mely egy fájlból minden második sort átmásol egy másik fájlba.
package hu.braininghub.zh_filehandling;

import java.util.List;

/**
 *
 * @author Zsolt
 */
public class Main {

    public static void main(String[] args) {
        ReadFromFile rf = new ReadFromFile();
        List<String> list = rf.readFromFileToList("file.txt");
        WriteToFile wf = new WriteToFile();
        wf.writeToFileEvrySecondLines("Copy.txt", list);
    }
}
