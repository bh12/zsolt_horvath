package zh2_vehicle;
//@author Zsolt Horváth

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class VehiclesList {

    private List<Vehicle> vehicles = new ArrayList<>();
    private File file = new File("vehiclesList.txt");

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public List<Vehicle> getVehicles() {
        return vehicles;
    }

    public void setVehicles(ArrayList<Vehicle> vehicles) {
        this.vehicles = vehicles;
    }

    public void report() {
        for (Vehicle vehicles : vehicles) {
            System.out.println(vehicles.toString());
        }

        getOldestVehicle();
        getYoungestVehicle();
        logOldestVehicle();
        logYoungestVehicle();
        sortedList();
        compareList();
    }

    public Vehicle getOldestVehicle() {
        final Optional<Vehicle> oldestVehicles = this.vehicles.stream()
                .sorted((vehicles1, vehicles2) -> vehicles2.compareTo(vehicles1))
                .findFirst();
        Vehicle oldestVehicle = oldestVehicles.get();
        return oldestVehicle;
    }

    public Vehicle getYoungestVehicle() {
        final Optional<Vehicle> youngerVehicles = this.vehicles.stream()
                .sorted((vehicles1, vehicles2) -> vehicles1.compareTo(vehicles2))
                .findFirst();
        Vehicle youngestVehicle = youngerVehicles.get();
        return youngestVehicle;
    }

    public List<Vehicle> compareList() {
        final List<Vehicle> sortedList = vehicles.stream().sorted((vehicles1, vehicles2) -> vehicles2.compareTo(vehicles1)).collect(Collectors.toList());
        return sortedList;
    }

    public void logOldestVehicle() {

        try (FileWriter fw = new FileWriter(getFile());) {

            fw.write(getOldestVehicle().toString() + System.lineSeparator());

        } catch (IOException e) {
            System.out.println("Nincs mit kimenteni");
        }

    }

    public void logYoungestVehicle() {

        try (FileWriter fw = new FileWriter(getFile());) {

            fw.write(getYoungestVehicle().toString() + System.lineSeparator());

        } catch (IOException e) {
            System.out.println("Nincs mit kimenteni");
        }

    }

    public void sortedList() {
        for (Vehicle vehicle : compareList()) {
            System.out.println(compareList().toString());
        }
        try (FileWriter fw = new FileWriter(getFile());) {

            fw.write(compareList().toString() + System.lineSeparator());

        } catch (IOException e) {
            System.out.println("Nincs mit kimenteni");
        }
    }

}
