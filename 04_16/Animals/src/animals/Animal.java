package animals;

import java.util.Objects;

public abstract class Animal extends Creature{
    private String type;
    private boolean domestic;
    private boolean edible;
    private boolean forFun;
    private boolean forSecure;

    public Animal(String type, boolean domestic, boolean edible, boolean forFun, boolean forSecure) {
        this.type = type;
        this.domestic = domestic;
        this.edible = edible;
        this.forFun = forFun;
        this.forSecure = forSecure;
    }
    
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isDomestic() {
        return domestic;
    }

    public void setDomestic(boolean domestic) {
        this.domestic = domestic;
    }

    public boolean isEdible() {
        return edible;
    }

    public void setEdible(boolean edible) {
        this.edible = edible;
    }

    public boolean isForFun() {
        return forFun;
    }

    public void setForFun(boolean forFun) {
        this.forFun = forFun;
    }

    public boolean isForSecure() {
        return forSecure;
    }

    public void setForSecure(boolean forSecure) {
        this.forSecure = forSecure;
    }

    @Override
    public String toString() {
        return "Animal{" + "type=" + type + ", domestic=" + domestic + ", edible=" + edible + ", forFun=" + forFun + ", forSecure=" + forSecure + '}';
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }
//if (a.equals(b)) { this = a; obj = b
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Animal other = (Animal) obj;
        if (this.domestic != other.domestic) {
            return false;
        }
        if (this.edible != other.edible) {
            return false;
        }
        if (this.forFun != other.forFun) {
            return false;
        }
        if (this.forSecure != other.forSecure) {
            return false;
        }
        if (!Objects.equals(this.type, other.type)) {
            return false;
        }
        return true;
    }
    
    public abstract void sayHello();
}
