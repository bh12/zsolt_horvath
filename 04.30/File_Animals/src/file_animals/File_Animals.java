//      Olvassuk be az „animals.csv” fájlt. Ebben található állatokról készítsünk statisztikát:
//a.    Írjuk ki hány állatot tartalmaz a fájl, illetve hány tulajdonságot (a nevet ne számoljuk bele)
//b.    Írjuk ki hány állatnak van szőre, hánynak nincs
//c.    Írjuk ki hány állatnak van tolla, hánynak nincs
//d.    Írjuk ki az adott lábszámhoz tartozó állatok darabszámát
//e.    Írjuk ki a class_type alapján a típusokhoz tartozó állatok darabszámát
//f.    Írjuk ki az összes nem lélegző állat nevét
//g.    Írjuk ki azon állatok neveit és az összes tulajdonságait, amik gerinces és ragadózó állatok.
package file_animals;

//@author Zsolt Horváth
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class File_Animals {

    public static List<Animal> animals = new ArrayList<>();

    public static void main(String[] args) {

        uploadAnimalList();
        System.out.println("------------");

        System.out.println("Állatok száma: " + animals.size());

        System.out.println("A szőrös állatok száma: " + isHairs() + "\nA szőrtelenek száma: " + (animals.size() - isHairs()));

        System.out.println("A tollas állatok száma: " + isFeathers() + "\nA toll nélküliek száma: " + (animals.size() - isFeathers()));

        numberOfLegs();

        numberOfClassTypeAnimals();

        isNotBreathingAnimals();
        System.out.println("\n");
        isBackBoneAndIsPredator();

    }

    public static void isBackBoneAndIsPredator() {
        System.out.println("A gerinces és ragadozó állatok nevei és tulajdonságaik: ");
        for (Animal a : animals) {
            if (a.getIsPredator().equals("1") && a.getIsBackbone().equals("1")) {
                System.out.println(a.toString());
            }
        }
    }

    public static void isNotBreathingAnimals() {
        System.out.println("A nem lélegző állatok nevei:");
        for (Animal a : animals) {
            if (a.getIsBreathes().equals("0")) {
                System.out.print(a.getName() + ",");
            }
        }
    }

    public static void numberOfClassTypeAnimals() {
        int classType1 = 0;
        int classType2 = 0;
        int classType3 = 0;
        int classType4 = 0;
        int classType5 = 0;
        int classType6 = 0;
        int classType7 = 0;
        for (Animal a : animals) {
            switch (a.getIsClass_type()) {
                case "1":
                    classType1++;
                    break;
                case "2":
                    classType2++;
                    break;
                case "3":
                    classType3++;
                    break;
                case "4":
                    classType4++;
                    break;
                case "5":
                    classType5++;
                    break;
                case "6":
                    classType6++;
                    break;
                case "7":
                    classType7++;
                    break;
            }
        }
        System.out.println("Az 1. osztályba tartozó állatok száma: " + classType1);
        System.out.println("Az 2. osztályba tartozó állatok száma: " + classType2);
        System.out.println("Az 3. osztályba tartozó állatok száma: " + classType3);
        System.out.println("Az 4. osztályba tartozó állatok száma: " + classType4);
        System.out.println("Az 5. osztályba tartozó állatok száma: " + classType5);
        System.out.println("Az 6. osztályba tartozó állatok száma: " + classType6);
        System.out.println("Az 7. osztályba tartozó állatok száma: " + classType7);
    }

    public static int isFeathers() {
        int counterOfFeathers = 0;
        for (Animal a : animals) {
            if (a.getIsFeathers().equals("1")) {
                counterOfFeathers++;
            }
        }
        return counterOfFeathers;
    }

    public static int isHairs() {
        int counterOfHairs = 0;
        for (Animal a : animals) {
            if (a.getIsHair().equals("1")) {
                counterOfHairs++;
            }
        }
        return counterOfHairs;
    }

    public static void uploadAnimalList() {
        try (FileReader fr = new FileReader("animals.csv");
                BufferedReader br = new BufferedReader(fr)) {
            br.readLine();
            String line = null;

            while ((line = br.readLine()) != null) {

                String[] parsedLine = line.split(";");

                Animal tmpAnimals = new Animal();

                tmpAnimals.setName(parsedLine[0]);
                tmpAnimals.setIsHair(parsedLine[1]);
                tmpAnimals.setIsFeathers(parsedLine[2]);
                tmpAnimals.setIsEggs(parsedLine[3]);
                tmpAnimals.setIsMilk(parsedLine[4]);
                tmpAnimals.setIsAirborne(parsedLine[5]);
                tmpAnimals.setIsAquatic(parsedLine[6]);
                tmpAnimals.setIsPredator(parsedLine[7]);
                tmpAnimals.setIsToothed(parsedLine[8]);
                tmpAnimals.setIsBackbone(parsedLine[9]);
                tmpAnimals.setIsBreathes(parsedLine[10]);
                tmpAnimals.setIsVenomous(parsedLine[11]);
                tmpAnimals.setIsFins(parsedLine[12]);
                tmpAnimals.setLegs(parsedLine[13]);
                tmpAnimals.setIsTail(parsedLine[14]);
                tmpAnimals.setIsDomestic(parsedLine[15]);
                tmpAnimals.setIsCatsize(parsedLine[16]);
                tmpAnimals.setIsClass_type(parsedLine[17]);
                animals.add(tmpAnimals);

            }

        } catch (IOException e) {
            System.out.println("File hiba");
        }
        for (Animal a : animals) {
            System.out.println(a.toString());
        }
    }

    public static void numberOfLegs() {
        int withoutLegs = 0;
        int with2legs = 0;
        int with4legs = 0;
        int with5legs = 0;
        int with6legs = 0;
        int with8legs = 0;
        for (Animal a : animals) {
            switch (a.getLegs()) {
                case "0":
                    withoutLegs++;
                    break;
                case "2":
                    with2legs++;
                    break;
                case "4":
                    with4legs++;
                    break;
                case "5":
                    with5legs++;
                    break;
                case "6":
                    with6legs++;
                    break;
                case "8":
                    with8legs++;
                    break;
            }
        }
        System.out.println("A láb nélüli állatok száma: " + withoutLegs);
        System.out.println("A kétlábú állatok száma: " + with2legs);
        System.out.println("A 4 lábú állatok száma: " + with6legs);
        System.out.println("A 5 lábú állatok száma: " + with5legs);
        System.out.println("A 6 lábú állatok száma: " + with6legs);
        System.out.println("A 8 lábú állatok száma: " + with8legs);
    }

}
