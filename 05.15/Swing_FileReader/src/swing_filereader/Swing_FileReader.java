// Manuálisan hozzunk létre egy fájlt, amibe valamilyen szöveget beírunk.
// Olvassuk be ezt a fájlt, és minden sort tegyünk be egy label-be, és jelenítsük meg.
// Használjunk egy Layout Manager-t.
// JFileChooser

package swing_filereader;


//@author Zsolt Horváth

public class Swing_FileReader {

    
    public static void main(String[] args) {
        DataLoader dl = new DataLoader();
        for(String s : dl.loadDataFromFile()){
            System.out.println(s);
        }
        CustomSwing cs = new CustomSwing("swing");
        cs.init();

    }

}
