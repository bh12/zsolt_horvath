package hu.braininghub.mapper;

import hu.braininghub.dto.ProductDTO;
import hu.braininghub.entity.ProductEntity;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-08-02T15:12:57+0200",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 1.8.0_241 (Oracle Corporation)"
)
public class ProductMapperImpl implements ProductMapper {

    @Override
    public ProductDTO toDTO(ProductEntity productEntity) {
        if ( productEntity == null ) {
            return null;
        }

        ProductDTO productDTO = new ProductDTO();

        return productDTO;
    }

    @Override
    public List<ProductDTO> toDTOList(List<ProductEntity> productEntity) {
        if ( productEntity == null ) {
            return null;
        }

        List<ProductDTO> list = new ArrayList<ProductDTO>( productEntity.size() );
        for ( ProductEntity productEntity1 : productEntity ) {
            list.add( toDTO( productEntity1 ) );
        }

        return list;
    }
}
