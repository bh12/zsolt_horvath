package banksimulation.model;

import java.util.Date;

public class Transfer {

    private Client source;

    private Client target;

    private long ammount;

    private Date dateOfCompletion;

    public Transfer() {

    }

    public Transfer(Client source, Client target, long ammount, Date dateOfCompletion) {
        this.source = source;
        this.target = target;
        this.ammount = ammount;
        this.dateOfCompletion = dateOfCompletion;
    }

    public Client getSource() {
        return source;
    }

    public void setSource(Client source) {
        this.source = source;
    }

    public Client getTarget() {
        return target;
    }

    public void setTarget(Client target) {
        this.target = target;
    }

    public long getAmmount() {
        return ammount;
    }

    public void setAmmount(long ammount) {
        this.ammount = ammount;
    }

    public Date getDateOfCompletion() {
        return dateOfCompletion;
    }

    public void setDateOfCompletion(Date dateOfCompletion) {
        this.dateOfCompletion = dateOfCompletion;
    }

    @Override
    public String toString() {
        return "Transfer{" + "source=" + source + ", target=" + target + ", ammount=" + ammount + ", dateOfCompletion=" + dateOfCompletion + '}';
    }

    

}
