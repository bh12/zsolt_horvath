package animals;

public class Chicken extends Animal implements CanFlyInterface {

    public Chicken(int age) {
        super("chicken", true, true, false, false);
        setAge(age);
        setLive(true);
        setLiveScore(10);        
    }

    @Override
    public void sayHello() {
        System.out.println("3.14 3.14 3.14");
    }

    @Override
    public String getDeathScream() {
        return "megyek a levesbe \n";
    }

    @Override
    public int getMaxHigh() {
        return 2;
    }
}
