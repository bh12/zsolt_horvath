package animals;


public class Turkey extends Animal implements AttackInterface, CanFlyInterface {
    public Turkey(int age) {
        super("turkey", true, true, false, true);
        setAge(age);
        setLive(true);
        setLiveScore(30);        
    }

    @Override
    public void sayHello() {
        System.out.println("gluuu");
    }

    @Override
    public String getDeathScream() {
        return "gluuugluuu \n";
    }

    @Override
    public int getAttackScore() {
        return 3;
    }

    @Override
    public void attack() {
        System.out.println("\t\t\tgluuuuuu");
    }

    @Override
    public int getMaxHigh() {
        return 1;
    }
    
}
