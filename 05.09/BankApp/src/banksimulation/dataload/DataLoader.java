
package banksimulation.dataload;

public interface DataLoader {
    
    public void loadInitialDataClient();
    public void loadInitialDataBank();
    public void loadInitialDataTransfer();
    
}
