package zh2_vehicle;

import java.time.LocalDate;

import java.util.Scanner;

//@author Zsolt Horváth
public class Main {

    public static Scanner sc = new Scanner(System.in);
    public static int option;
    public static VehiclesList vl = new VehiclesList();

    public static void main(String[] args) {
        startApp();

    }

    public static void startApp() {
        System.out.println("Kérem válasszon az alábbi lehetőségek közül:");
        System.out.println("1: Termék hozzáadása (ADD kulcsszó).");
        System.out.println("2: Termékek listázása: (REPORT kulcsszó).");
        System.out.println("3: Kilépés.");

        option = sc.nextInt();

        switch (option) {
            case 1:
                add();

                break;
            case 2:
                vl.report();
                startApp();
                break;
            case 3:
                break;

        }
    }

    public static void add() {
        System.out.println("Adja meg a típust (Car,Airplane,Train):");
        String vehicle = sc.next();
        System.out.println("Adja meg a gyártási dátumot:");
        System.out.println("Év: ");
        int year = sc.nextInt();
        System.out.println("Hónap:");
        int month = sc.nextInt();
        System.out.println("Nap:");
        int day = sc.nextInt();

        LocalDate localDate = LocalDate.of(year, month, day);
        System.out.println("Adja meg a tömegét kg-ban:");
        int weigth = sc.nextInt();

        if ("Car".equals(vehicle)) {
            Vehicle car = new Car();
            car.setLocalDate(localDate);
            car.setWeigth(weigth);
            car.setPlateNumber(randomPlateNumber());
            vl.getVehicles().add(car);

        }
        if ("Airplane".equals(vehicle)) {
            Vehicle airplane = new Airplane();
            airplane.setLocalDate(localDate);
            airplane.setWeigth(weigth);
            airplane.setPlateNumber(randomPlateNumber());
            vl.getVehicles().add(airplane);

        }
        if ("Train".equals(vehicle)) {
            Vehicle train = new Train();
            train.setLocalDate(localDate);
            train.setWeigth(weigth);
            train.setPlateNumber(randomPlateNumber());
            vl.getVehicles().add(train);

        }
        System.out.println("Sikeres hozzáadás.");

        startApp();

    }

    public static int randomPlateNumber() {
        int number = (int) (Math.random() * 1000) + 1;
        return number;
    }

}
