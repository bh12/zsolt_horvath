//3.Opcional gyakorlas: Keszitsen kincskereso alkalmazast amivel lepkedni lehet.
//Lepkedni barmilyen iranyba lehet, atlosan is.
//Eloszor kerjen be a felhasznalotol egy NxM-es palyameretet. Generaljon veletlenszeruen palyat.
//A palyameret teruletenek a minimum negyedere maximum felere generaljon kincseket.
//Lepni barmilyen iranyba lehet. Jelezze ki a felhasznalonak ha kincset talalt.
//Egy lepesnel jelezze ki a felhasznalonak mennyi kincs van a lepes kornyerzeteben
//(ha pont kincsre leptel azt nem szamoljuk ebbe bele).
//A jatek addig tart amig az osszes kincset meg nem talaltuk a palyan.
//Ekkor irja ki a jatekosnak a lepesek es a megtalalt kincsek szamat.
package pkg6.ora_feladatok;

//@author Zsolt Horváth
import java.util.Random;
import java.util.Scanner;

public class Task3_Kincskereso {

    public static void main(String[] args) {
        //térkép 
        boolean[][] stage = createField(fieldSize());
        //kincsek elhelyezése
        int numberOfTreasure = calculateNumberOfTreasure(stage);
        fillField(stage, numberOfTreasure);
        // ellenőrzés
//        printField(stage);

        int stepCounter = 0;
        int treasureCounter = 0;
        do {
            // tipp bekérése
            int[] userTipXY = userTip();
            // kincs keresése
            if (stage[userTipXY[0]][userTipXY[1]]) {
                System.out.println("Kincset találtál!");
                treasureCounter++;
                numberOfTreasure--;
                stepCounter++;
            } else {
                stepCounter++;
                System.out.println("Itt nem volt kincs.");
            }

        } while (numberOfTreasure != 0);
        System.out.println("Talált kincsek száma: " + treasureCounter);
        System.out.println("Sikeres lépéseid száma: " + stepCounter);
    }

    public static int[] fieldSize() {
        Scanner sc = new Scanner(System.in);
        int[] fieldWidthLength = new int[2];

        System.out.println("A térkép szélessége: ");
        fieldWidthLength[0] = sc.nextInt();
        System.out.println("A térkép hosszúság: ");
        fieldWidthLength[1] = sc.nextInt();
        return fieldWidthLength;
    }

    public static boolean[][] createField(int[] fieldSize) {
        int row = fieldSize[0];
        int column = fieldSize[1];
        boolean[][] stage = new boolean[row][column];
        return stage;
    }

    public static int calculateNumberOfTreasure(boolean[][] field) {
        int fieldArea = field.length * field[0].length;
        int treasureMin = fieldArea / 4;
        System.out.println("min: " + treasureMin);
        int treasureMax = fieldArea / 2;
        System.out.println("max: " + treasureMax);
        int numberOfTreasure = (int) (Math.random() * (treasureMax - treasureMin + 1)) + treasureMin;
        return numberOfTreasure;
    }

    public static void fillField(boolean[][] field, int numberOfTreasure) {
        int rows = field.length;
        int columns = field[0].length;
        for (int counter = 0; counter < numberOfTreasure; counter++) {
            int randomRow;
            int randomColumn;
            do {
                randomRow = new Random().nextInt(rows);
                randomColumn = new Random().nextInt(columns);
            } while (field[randomRow][randomColumn]);
            field[randomRow][randomColumn] = true;
        }
    }

    public static void printField(boolean[][] field) {
        for (int outerCounter = 0; outerCounter < field.length; outerCounter++) {
            System.out.println("");
            for (int innerCounter = 0; innerCounter < field[outerCounter].length; innerCounter++) {
                if (field[outerCounter][innerCounter] == true) {
                    System.out.print("  X  ");
                } else {
                    System.out.print("  O  ");
                }
            }
            System.out.println("");
        }
    }

    public static int[] userTip() {
        Scanner sc = new Scanner(System.in);
        int[] tippArray = new int[2];

        System.out.println("Adja meg a sor számát: ");
        tippArray[0] = sc.nextInt();
        System.out.println("Adja meg az oszlop számát: ");
        tippArray[1] = sc.nextInt();
        return tippArray;
    }
}
