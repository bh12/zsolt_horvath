// írj programot (objektum orientáltan megtervezve), ami képes nyilvántartani műszaki cikkeket az IKEAM áruház számára.
// Minden műszaki cikknek pontosan egy típusa (olcsó, átlagos, drága, luxus, népszerű) van,
// továbbá van gyártója (String), vonalkódja, ára (int) és tudjuk a regisztrálás dátumát (ami a felvétel napja).
// A termékek a vonalkódok sorszáma alapján összehasonlíthatók. Ezzel a tulajdonsággal minden termék rendelkezik.
// A műszaki cikkeknek 3 speciális típusa van, minden termék ebben a 3 csoport valamelyikében foglal helyet:
// konyhai eszközök,  szórakoztató eszközök és szépségápolással kapcsolatos gépek.
// A konyhai eszközöket fel lehet emelni és be lehet kapcsolni.
// A szórakoztató eszközöket is be lehet kapcsolni,
// viszont 5 bekapcsolás után valamilyen hibát jeleznek (saját kivételosztály szükséges).
// A harmadik csoportnak van súlya kg-ban.
// A raktárba érkezhetnek termékek és el is lehet őket adni. Az alábbi parancsokat a konzolon lehet megadni:
// ADD vonalkód típus gyártó ár speciális_csoport → egy termék felvétele
// REMOVE vonalkód → termék törlése
// REPORT → report generálása
//
// REPORT esetén az alábbi adatok jelennek meg a konzolon:
// Hány termék lett kimentve?
// Hány olyan szórakoztató eszköz van, ami még legalább 2 bekapcsolást bír?
package zh_ikeam;

//@author Zsolt Horváth
import java.util.Scanner;


public class Main {
    
    public static Scanner sc = new Scanner(System.in);
    public static Storage storage = new Storage();
    public static int option;
    
    public static void main(String[] args) {
        
        startApp();
    }
    
    public static void startApp() {
        System.out.println("Kérem válasszon az alábbi lehetőségek közül:");
        System.out.println("1: Termék hozzáadása (ADD kulcsszó).");
        System.out.println("2: Termék eltávolítása (REMOVE kulcsszó).");
        System.out.println("3: Hiba jelentése (REPORT kulcsszó).");
        System.out.println("4: Kilépés.");
        
        option = sc.nextInt();
        
        switch (option) {
            case 1:
                add();
                
                break;
            case 2:
                remove();
                
                break;
            case 3:
                report();
                break;
            case 4:
                break;
            
        }
    }
    
    public static void add() {
        
        System.out.println("Adja meg a vonalkódot:");
        int barCode = sc.nextInt();
        System.out.println("Adja meg a típusát:");
        Attribution at = Attribution.valueOf(sc.next().toUpperCase());
        System.out.println("Adja meg a gyártót:");
        String producer = sc.next();
        System.out.println("Adja meg az árat:");
        int price = sc.nextInt();
        System.out.println("Adja meg a termékcsoportot:");
        String name = sc.next();
        
        if ("Kitchen".equals(name)) {
            Product kitchen = new Kitchen();
            kitchen.setBarCode(barCode);
            kitchen.setAt(at);
            kitchen.setProducer(producer);
            kitchen.setPrice(price);
            storage.products.add(kitchen);
            
        }
        if ("Fun".equals(name)) {
            Product fun = new Fun();
            fun.setBarCode(barCode);
            fun.setAt(at);
            fun.setProducer(producer);
            fun.setPrice(price);
            storage.products.add(fun);
            
        }
        if ("Beauty".equals(name)) {
            Product beauty = new Beauty();
            beauty.setBarCode(barCode);
            beauty.setAt(at);
            beauty.setProducer(producer);
            beauty.setPrice(price);
            storage.products.add(beauty);
            
        }
        System.out.println("Sikeres hozzáadás.");
        System.out.println(storage.products.size());
        startApp();
    }
    
    public static void remove() {
        System.out.println("Kérem,adja meg a törölni kívánt termék vonalkódját:");
        int removeCode = sc.nextInt();
        storage.products.removeIf(br -> br.getBarCode() == removeCode);
        
        System.out.println(storage.products.size());
    }
    
    public static void report() {
        int counterOfMore2Turn = 0;
        storage.products.stream()
                .forEach(System.out::println);
        
        for (Product p : storage.products) {
            if (p instanceof CanTurnOnOff) {
                CanTurnOnOff cto = (CanTurnOnOff) p;
                if (cto.getTurnCount() >= 2) {
                    counterOfMore2Turn++;
                }
            }
        }
        System.out.println(counterOfMore2Turn);
    }
    
}
