package hu.braininghub.mapper;

import hu.braininghub.dto.OrderDTO;
import hu.braininghub.entity.OrderEntity;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-08-02T15:12:57+0200",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 1.8.0_241 (Oracle Corporation)"
)
public class OrderMapperImpl implements OrderMapper {

    @Override
    public OrderDTO toDTO(OrderEntity orderEntity) {
        if ( orderEntity == null ) {
            return null;
        }

        OrderDTO orderDTO = new OrderDTO();

        return orderDTO;
    }

    @Override
    public List<OrderDTO> toDTOList(List<OrderEntity> orderEntity) {
        if ( orderEntity == null ) {
            return null;
        }

        List<OrderDTO> list = new ArrayList<OrderDTO>( orderEntity.size() );
        for ( OrderEntity orderEntity1 : orderEntity ) {
            list.add( toDTO( orderEntity1 ) );
        }

        return list;
    }
}
