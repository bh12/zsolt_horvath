// A program olvassa be a „scanner_text.txt” fájl tartalmát és írja ki a konzolra.
package file;

//@author Zsolt Horváth

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Four {

    public static void main(String[] args) {
        File f = new File("scanner_text.txt");

        char[] charBuffer = new char[40];
        try (FileReader fr = new FileReader(f)) {
            int readCounter = fr.read(charBuffer);
            while (readCounter != -1) {
                System.out.println(new String(charBuffer, 0, readCounter));
                readCounter = fr.read(charBuffer);
            }
        } catch (IOException e) {
            System.out.println("File hiba");
        }

    }

}
