// Írj Swing-es programot MVC-t megvalósítva: 
//  a felhasználói felület egy JTextArea és egy JButton komponensből áll. 
//  A JTextArea középen legyen, a nyomógomb pedig alatta, és töltse ki a rendelkezésre álló területet. 
//  A nyomógomb által keltett ActionEvent eseményeket logolni kell a JTextArea komponensre 
//  a keletkezés időpontja alapján (pl.: 1. [2019-05-03 19:51:15]: Button pressed). (10 pont)
// Az eseményeket egy txt fájlban is rögzítsd!  
//  A fájlban az ugyanahhoz az órához tartozó bejegyzéseket egy csoportnak tekintjük. 
//  A csoportokat egy üres sor választja el. Az eseményeket sorszámmal is lásd el mindkét kimenet esetében. (5 pont)
// Tip: A karakterfolyamnak van olyan konstruktora ahol állítható az állományhoz történő hozzáfűzés képessége.  2020-05-03 19:51:15
//  2020-05-03 19:53:15
//  2020-05-03 19:54:15
// 
//  2020-05-03 20:01:15

package swingzh;

import swingzh.Controller.Controller;



//@author Zsolt Horváth

public class App {

    
    public static void main(String[] args) {
        Controller c = new Controller();
    }

}
