package swingzh.view;
//@author Zsolt Horváth

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.JButton;
import javax.swing.JFrame;

import javax.swing.JPanel;

import javax.swing.JTextArea;
import swingzh.Controller.Controller;

public class AppView extends JFrame {

    private Controller controller;

    private JPanel panel;
    private JTextArea text;
    private JButton button;

    public AppView(Controller controller) {
        this.controller = controller;
    }
    public void init() {
        setUpWindow();
        setUpSreen();
        setUpButton();
        showWindow();
    }

    public void setUpWindow() {
        this.setSize(500, 500);
        this.setLocationRelativeTo(null);
        this.setTitle("ZH");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        panel = new JPanel();
        panel.setLayout(new GridBagLayout());
        add(panel);
    }

    private void showWindow() {
        this.setVisible(true);
    }

    public void setUpSreen() {
        this.text = new JTextArea(1, 25);
        Font f = new Font("Arial", Font.BOLD, 20);
        this.text.setFont(f);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbc.gridx = 2;
        gbc.gridy = 1;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(this.text, gbc);
    }

    private void setUpButton() {
        this.button = new JButton("Log");
        this.button.addActionListener(event -> {controller.handleAddButton();});
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbc.gridx = 2;
        gbc.gridy = 2;
        gbc.fill = GridBagConstraints.BOTH;
        panel.add(this.button, gbc);
    }
    
     public void setScreenText(String text) {
        this.text.setText(text);
    }
     
     public String getText(){
         return text.getText();
     }

}
